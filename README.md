# Frontend for a F1 search engine

- WebUI which displays a filterable list/grid of Formula 1 drivers

    - The text-search through driver names is performed leniently using [Fuzzy Search (via FuseJs)](https://fusejs.io/) and the results are sorted with the best matches on the top

- Upon clicking on a driver's name, a drive-profile page is displayed. Said driver-profile is displayed with:

    - Loader animation support
    - Personal details to the left
    - The racing results to the right as a grid with
        - Sortable properties
        - Filterable properties
    - Return to previous page button
    - Support for deep-linking so that the url will be bookmarkable and anyone with the link can view each
      driver's profile

# Building & Running the UI

- npm install
- npm run start:prod
- localhost:4200

# Technical Specifications

Developed using:

- Angular9 (9.1.0-next.3)
- Typescript 3.7.5
- SCSS for styling
- Using ngRx for state management (Redux) with smart-container/dumb-components
- The UI has been kept bare-bonish on purpose. Consciously refrained from using third-party libs such as bootstrap, material, etc

# Points of Interest

- The Ergast API doesn't seem to support filtering through drivers using a contains-string type of query. We thus download snapshots of the entire page-collections for each view and perform the filtering solely on the browser-side of things

# Ergast API Behaviour

- API endpoints used:

      https://ergast.com/mrd/

- Documentation detailing how to retrieve any info about Drivers:

      https://ergast.com/mrd/methods/drivers/

      The 'limit' parameter is subject to a hardcoded max value of '1000' in the ergast server for ostensibly all queries.


# Git Branch Structure

Git branches have been structured in a "versioned" gitflow manner ala:

      master
      |
      \ v000.000.001
           \- production
           \- integration
           \- develop

# GitLab CI/CD

TODO (also consider adding a build script for local IIS deployment if possible!)

# Development Environment

- VSCode
- Prettier
- MoveTS
- Refactor this!
- ESLint has been employed for static code analysis per

       https://medium.com/@myylow/how-to-keep-the-airbnb-eslint-config-when-moving-to-typescript-1abb26adb5c6

# Redux State

Landing Page  /drivers

- The current filter-text in the searchbox
- The currently selected driver-row (by driverid)

Driver Profile Page   /drivers/:driver-id

- The currently selected driver (driver-id)
- Grid column-filters statuses
- Grid column-sorting-order statuses

# Notes

-  If the driver-url supplied is invalid

/profile/:driver-name

We handle this gracefully by displaying a human readable error message in a new page

# Resources

- [In-app navigation with routing](https://angular.io/tutorial/toh-pt5#add-in-app-navigation-with-routing)

- [NgRx Router Store | Reduce & Select Route Params](https://medium.com/simars/ngrx-router-store-reduce-select-route-params-6baff607dd9)

     import { Router } from '@angular/router';
     this.router.navigateByUrl('/drivers/' + driveId)

- [Immortal Technique: Extracting Multiple Observables with the async pipe in angular](https://medium.com/@ofirrifo/extract-multiple-observables-with-the-async-pipe-in-angular-b119d22f8e05)

- [Sample real world Angular app (ver 5)](https://stackblitz.com/edit/angular-realworld)

- [Declarative Title Updater with Angular and ngrx](https://alligator.io/angular/title-updater/)

- [Angular-Leaning Functional Programming Primer](https://medium.com/angular-in-depth/functional-programming-in-simple-terms-abcef30a2ad1)
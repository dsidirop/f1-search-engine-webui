const ERGAST_URL = 'http://ergast.com/';
const ERGAST_MOCK_F1_PATH = '/api/f1/*'; //                     /api/f1/drivers.json?limit=1000
const ERGAST_MOCK_F1_DRIVERS_PATH = '/api/f1/*/*'; //           /api/f1/drivers/acheson.json
const ERGAST_MOCK_F1_DRIVERS_RESULTS_PATH = '/api/f1/*/*/*'; // /api/f1/drivers/acheson/results.json?limit=1000

const PROXY_CONFIG = [
  {
    context: [ERGAST_MOCK_F1_PATH, ERGAST_MOCK_F1_DRIVERS_PATH, ERGAST_MOCK_F1_DRIVERS_RESULTS_PATH],
    target: ERGAST_URL,
    secure: false,
    logLevel: 'debug',
    changeOrigin: true,
  },
];

module.exports = PROXY_CONFIG;

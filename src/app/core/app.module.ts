import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HomeModule } from '@app-home/home.module';
import { HttpCancelInterceptor } from '@app-shared/interceptor';
import { SharedModule } from '@app-shared/shared.module';
import { AppRoutingModule } from './app-routing.module';
import { AppStoreModule } from './app-store.module';
import { AppHeaderComponent } from './components/dummies';
import { AppSidebarComponent } from './components/dummies/sidebar/app-sidebar.component';
import { AppComponent } from './components/smarties';

@NgModule({
  declarations: [
    AppComponent,
    AppHeaderComponent,
    AppSidebarComponent
  ],
  imports: [
    //libs
    BrowserModule,
    HttpClientModule,
    //app modules
    HomeModule,
    SharedModule,
    // DriverProfileModule //lazyloaded
    //store
    AppStoreModule,
    //routing
    AppRoutingModule
  ],
  providers: [
    {
      multi: true,
      provide: HTTP_INTERCEPTORS,
      useClass: HttpCancelInterceptor
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { NgModule } from '@angular/core';
import { environment } from '@app-env/environment';
import { CustomRouterStateSerializer } from '@app-shared/routing';
import { RootEffects } from '@app-shared/store/root/root.effects';
import * as fromRoot from '@app-shared/store/root/root.reducer';
import { RouterEffects } from '@app-shared/store/router/router.effects';
import * as fromRouter from '@app-shared/store/router/router.reducer';
import { EffectsModule } from '@ngrx/effects';
import { RouterStateSerializer, StoreRouterConnectingModule } from '@ngrx/router-store';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

export const StoreDevToolsInstrumentationModule = environment.production
  ? []
  : StoreDevtoolsModule.instrument({ maxAge: 50 });

@NgModule({
  imports: [
    StoreRouterConnectingModule.forRoot(),
    //root
    StoreModule.forRoot(fromRoot.rootReducers, {
      metaReducers: fromRoot.metaReducers,
      runtimeChecks: {
        strictStateImmutability: true,
        strictActionImmutability: true
      }
    }),
    EffectsModule.forRoot([RootEffects]), // its vital to plug in the root state for features to work
    //features
    StoreModule.forFeature(fromRouter.routerFeatureKey, fromRouter.reducersMap),
    //effects
    EffectsModule.forFeature([RouterEffects]),
    //devtools
    StoreDevToolsInstrumentationModule //deadlast
  ],
  providers: [
    // The RouterStateSnapshot provided by the Router is a large complex structure
    // A custom RouterStateSerializer is used to parse the RouterStateSnapshot provided
    // by @ngrx/router-store to include only the desired pieces of the snapshot the title
    { provide: RouterStateSerializer, useClass: CustomRouterStateSerializer }
  ],
  exports: [StoreModule]
})

export class AppStoreModule { }

import { Component, OnInit } from '@angular/core';
import { NavigationStart, Router } from '@angular/router';
import { AppHeaderComponent, AppSidebarComponent } from '@app-core/components/dummies';
import { AppBaselineComponent } from '@app-shared/components';
import { GlobalConfigurationService } from '@app-shared/configuration';
import { GlobalConfiguration } from '@app-shared/models';
import { HttpCancelService } from '@app-shared/services';
import * as fromConfigurationStore from '@app-shared/store/configuration';
import { UpsertConfiguration } from '@app-shared/store/configuration';
import { select, Store } from '@ngrx/store';
import { filter, takeUntil } from 'rxjs/operators';
// import "zone.js/dist/zone-error";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [AppHeaderComponent, AppSidebarComponent]
})
export class AppComponent extends AppBaselineComponent implements OnInit {
  public constructor(
    private readonly router: Router,
    private readonly httpCancelService: HttpCancelService,
    private readonly globalConfigurationService: GlobalConfigurationService,

    private readonly configurationsStore$: Store<fromConfigurationStore.ConfigurationsState>
  ) {
    super();
  }

  public ngOnInit(): void {
    this
      .wireUpGlobalConfigurationService()
      .wireUpRouteNavigationSpyForRequestCancelations()
      .dispatchUpsertGlobalConfiguration();
  }

  private wireUpGlobalConfigurationService(): AppComponent {
    this.configurationsStore$.pipe(
      select(fromConfigurationStore.selectDefaultConfiguration),
      filter(x => !!x), //vital
      takeUntil(this.ngUnsubscribe$)
    ).subscribe(
      newConfiguration => this.globalConfigurationService.updateAll(newConfiguration as GlobalConfiguration)
    );

    return this;
  }

  private dispatchUpsertGlobalConfiguration(): AppComponent {
    this
      .configurationsStore$
      .dispatch(UpsertConfiguration({
        configuration: { ...this.globalConfigurationService.globalConfiguration }
      }));

    return this;
  }

  private wireUpRouteNavigationSpyForRequestCancelations(): AppComponent { //0
    this.router.events
      .pipe(filter(event => event instanceof NavigationStart))
      .subscribe(() => this.httpCancelService.cancelPendingRequests());

    return this;

    //0 stackoverflow.com/a/49797920/863651
  }
}

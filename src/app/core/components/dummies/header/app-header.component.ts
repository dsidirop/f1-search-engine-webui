import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AppBaselineComponent, LoadingAnimationDancingDotsComponent } from '@app-shared/components';
import { RouterFacade } from '@app-shared/store/router/router.facade';
import * as fromCoreStatusStore from '@app-shared/store/core-status';
import { CoreStatus } from '@app-shared/store/core-status';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-header',
  templateUrl: './app-header.component.html',
  styleUrls: ['./app-header.component.scss'],
  providers: [LoadingAnimationDancingDotsComponent],
  encapsulation: ViewEncapsulation.None
})
export class AppHeaderComponent extends AppBaselineComponent implements OnInit {
  public coreStatus$ = null as Observable<CoreStatus>;

  public constructor(
    //services
    private readonly routerFacade: RouterFacade,
    //stores
    private readonly coreStatusStore$: Store<fromCoreStatusStore.CoreStatusState>
  ) {
    super();
  }

  public ngOnInit(): void {
    this.wireUpCoreStatus();
  }

  public backButtonClicked = (): void => this.routerFacade.navigateBack();

  private wireUpCoreStatus(): AppHeaderComponent {
    this.coreStatus$ = this.coreStatusStore$.pipe(
      select(fromCoreStatusStore.selectDefaultCoreStatus),
      takeUntil(this.ngUnsubscribe$)
    );

    return this;
  }
}

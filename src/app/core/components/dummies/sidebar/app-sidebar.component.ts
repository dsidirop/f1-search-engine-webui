import { Component } from '@angular/core';
import { AppBaselineComponent } from '@app-shared/components';
import { ViewModeDirective } from '@app-shared/directives/view-mode.directive';
import { RouterFacade } from '@app-shared/store/router/router.facade';

@Component({
  selector: 'app-sidebar',
  templateUrl: './app-sidebar.component.html',
  styleUrls: ['./app-sidebar.component.scss'],
  providers: [ViewModeDirective]
})
export class AppSidebarComponent extends AppBaselineComponent {

  public constructor(private readonly routerFacade: RouterFacade) {
    super();
  }

  public logoClicked = (): void => this.routerFacade.navigateTo({ path: ['/'] });
}

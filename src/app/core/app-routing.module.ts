import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { StoreModule } from '@ngrx/store';
import { QuicklinkStrategy } from 'ngx-quicklink';

const rootRouting: ModuleWithProviders = RouterModule.forRoot(
  [
    //{ path: '' } specified in homeroutingmodule
    {
      path: 'drivers',
      loadChildren: () => import('./../driver-profile/driver-profile.module').then(m => m.DriverProfileModule)
    },
    {
      path: 'error',
      loadChildren: () => import('./../message-board/message-board.module').then(m => m.MessageBoardModule)
    },
    {
      path: '**',
      redirectTo: ''
    }
  ],
  {
    useHash: true,
    preloadingStrategy: QuicklinkStrategy
  }
);

@NgModule({
  imports: [rootRouting],
  exports: [StoreModule, RouterModule]
})

export class AppRoutingModule { }

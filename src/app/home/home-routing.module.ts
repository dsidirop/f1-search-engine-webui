import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HomeComponent } from './components/smarties';

const homeRouting: ModuleWithProviders = RouterModule.forChild([
  {
    path: '',
    pathMatch: 'full',
    component: HomeComponent
  }
]);

@NgModule({
  imports: [homeRouting],
  exports: [RouterModule]
})

export class HomeRoutingModule { }

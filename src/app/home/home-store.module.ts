import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import * as fromHome from './store/home-status';

@NgModule({
  imports: [StoreModule.forFeature(fromHome.homeFeatureKey, fromHome.reducer)],
  exports: [StoreModule]
})

export class HomeStoreModule { }

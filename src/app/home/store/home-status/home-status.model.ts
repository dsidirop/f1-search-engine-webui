export interface HomeStatus {
  id: string;
  driversFilterTerm: string;
}

import { Update } from '@ngrx/entity';
import { createAction, props, union } from '@ngrx/store';
import { HomeStatus } from './home-status.model';

export const addHomeStatus = createAction('[Home] Add Home Status', props<{ homeStatus: HomeStatus }>());
export const deleteHomeStatus = createAction('[Home] Delete Home Status', props<{ id: string }>());
export const upsertHomeStatus = createAction('[Home] Upsert Home Status', props<{ homeStatus: Partial<HomeStatus> }>());
export const updateHomeStatus = createAction('[Home] Update Home Status', props<{ homeStatus: Update<HomeStatus> }>());
export const addHomesStatuses = createAction('[Home] Add Homes Statuses', props<{ homesStatuses: HomeStatus[] }>());
export const loadHomesStatuses = createAction('[Home] Load Homes Statuses', props<{ homesStatuses: HomeStatus[] }>());
export const clearHomesStatuses = createAction('[Home] Clear Homes Statuses');
export const upsertHomesStatuses = createAction('[Home] Upsert Homes Statuses', props<{ homesStatuses: HomeStatus[] }>());
export const updateHomesStatuses = createAction('[Home] Update Homes Statuses', props<{ homesStatuses: Update<HomeStatus>[] }>());
export const deleteHomesStatuses = createAction('[Home] Delete Homes Statuses', props<{ ids: string[] }>());

const actions = union({
    addHomeStatus,
    deleteHomeStatus,
    upsertHomeStatus,
    updateHomeStatus,
    addHomesStatuses,
    loadHomesStatuses,
    clearHomesStatuses,
    upsertHomesStatuses,
    updateHomesStatuses,
    deleteHomesStatuses
});

export type ActionsTypeGroup = typeof actions;

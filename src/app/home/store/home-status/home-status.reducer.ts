import { EStandardStoreKeys } from '@app-shared/models/enums';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { createFeatureSelector, createReducer, createSelector, on } from '@ngrx/store';
import * as HomeActions from './home-status.actions';
import { HomeStatus } from './home-status.model';

export const homeFeatureKey = 'homeStatus'; //typically just one status the default one

export type HomesStatusesState = EntityState<HomeStatus>

export const adapter: EntityAdapter<HomeStatus> = createEntityAdapter<HomeStatus>({
  selectId: (configuration: HomeStatus) => configuration?.id,
  sortComparer: false
});

export const initialState: HomesStatusesState = adapter.getInitialState({
  ids: [EStandardStoreKeys.Default],
  entities: {
    [EStandardStoreKeys.Default]: {
      id: EStandardStoreKeys.Default,
      driversFilterTerm: ''
    } as HomeStatus
  }
} as HomesStatusesState);

const homeReducer = createReducer(
  initialState,
  on(HomeActions.addHomeStatus, (state, action) => adapter.addOne(action.homeStatus, state)),
  on(HomeActions.addHomesStatuses, (state, action) => adapter.addMany(action.homesStatuses, state)),
  on(HomeActions.deleteHomeStatus, (state, action) => adapter.removeOne(action.id, state)),
  on(HomeActions.upsertHomeStatus, (state, action) => {
    const newHomeStatus = {
      ...action.homeStatus,
      id: action.homeStatus.id ?? EStandardStoreKeys.Default
    } as HomeStatus;
    return adapter.upsertOne(newHomeStatus, state);
  }),
  on(HomeActions.updateHomeStatus, (state, action) => adapter.updateOne(action.homeStatus, state)),
  on(HomeActions.loadHomesStatuses, (state, action) => adapter.addAll(action.homesStatuses, state)),
  on(HomeActions.clearHomesStatuses, (state) => adapter.removeAll(state)),
  on(HomeActions.deleteHomesStatuses, (state, action) => adapter.removeMany(action.ids, state)),
  on(HomeActions.upsertHomesStatuses, (state, action) => adapter.upsertMany(action.homesStatuses, state)),
  on(HomeActions.updateHomesStatuses, (state, action) => adapter.updateMany(action.homesStatuses, state))
);

export function reducer(state: HomesStatusesState | undefined, action: HomeActions.ActionsTypeGroup): HomesStatusesState {
  return homeReducer(state, action);
}

export const selectHomesState = createFeatureSelector<HomesStatusesState>(homeFeatureKey);

export const { selectIds, selectEntities, selectAll, selectTotal } = adapter.getSelectors(selectHomesState);

export const selectDefault = createSelector(
  selectEntities,
  (entities) => entities[EStandardStoreKeys.Default]
);

export const selectDriversFilterTerm = createSelector(
  selectDefault,
  (defaultEntity) => defaultEntity?.driversFilterTerm
);

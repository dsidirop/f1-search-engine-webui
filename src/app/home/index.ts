// start:ng42.barrel
export * from './components/dummies';
export * from './home-routing.module';
export * from './components/smarties';
export * from './store';
// end:ng42.barrel


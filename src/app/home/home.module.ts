import { ScrollingModule } from '@angular/cdk/scrolling';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '@app-shared';
import { DriversListComponent } from './components/dummies/drivers-list';
import { HomeComponent } from './components/smarties/home';
import { HomeRoutingModule } from './home-routing.module';
import { HomeStoreModule } from './home-store.module';

@NgModule({
  imports: [
    FormsModule,
    SharedModule,
    HomeStoreModule,
    ScrollingModule,
    HomeRoutingModule
  ],
  declarations: [
    HomeComponent,
    DriversListComponent
  ]
})

export class HomeModule { }

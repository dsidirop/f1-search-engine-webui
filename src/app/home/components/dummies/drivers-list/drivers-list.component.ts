import { Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';
import { LoadingAnimationDancingDotsComponent } from '@app-shared/components/dummies/loading-animation-dancing-dots';
import { FuzzySearchResultChunkSplitterPipe, SingleComponent } from '@app-shared/pipes/fuzzy-search-result-chunk-splitter.pipe';
import { FuzzySearchResult } from '@app-shared/services/fuzzy-search/fuzzy-search-result';
import { Driver } from '@app-shared/store/drivers';

export interface DriverClickedEventArgs {
  info: Driver;
}

@Component({
  selector: 'app-drivers-list',
  templateUrl: './drivers-list.component.html',
  styleUrls: ['./drivers-list.component.scss'],
  providers: [FuzzySearchResultChunkSplitterPipe, LoadingAnimationDancingDotsComponent],
  encapsulation: ViewEncapsulation.None
})
export class DriversListComponent {

  @Input()
  public loading = false;

  @Input()
  public fuzzySearchResults = [] as FuzzySearchResult<Driver>[];

  @Input()
  public viewportItemCount = 10; //todo  autorecalculate viewport height!

  @Output()
  public driverClicked = new EventEmitter<DriverClickedEventArgs>();

  public itemClicked = (driverInfo: Driver): void =>
    this.driverClicked.emit({ info: driverInfo });

  public chunkTrackByFn = (index: number, chunk: SingleComponent): number =>
    chunk?.start;

  public searchResultTrackByFn = (index: number, fuzzySearchResult: FuzzySearchResult<Driver>): string =>
    fuzzySearchResult?.item?.id;
}

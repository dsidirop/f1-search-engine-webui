import { Component, OnInit } from '@angular/core';
import { DriverClickedEventArgs, DriversListComponent } from '@app-home/components/dummies';
import * as fromHomeStatusesStore from '@app-home/store/home-status';
import { AppSmartBaselineComponent } from '@app-shared/components/app-smart-baseline.component';
import { DebouncedInputChangedEventArgs, DebounceInputConfig, DebounceInputDirective, FocusOnLoadDirective } from '@app-shared/directives';
import { FuzzySearchOptions, FuzzySearchResult, FuzzySearchService } from '@app-shared/services';
import { UtilsService } from '@app-shared/services/utils.service';
import * as fromConfigurationStore from '@app-shared/store/configuration';
import * as fromCoreStatusStore from '@app-shared/store/core-status';
import * as fromDriversStore from '@app-shared/store/drivers';
import { Driver } from '@app-shared/store/drivers';
import { RouterFacade } from '@app-shared/store/router/router.facade';
import { select, Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
import { filter, map, take, takeUntil } from 'rxjs/operators';

// stackblitz.com/angular/rddkerpbbde

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers: [DriversListComponent, FocusOnLoadDirective, DebounceInputDirective]
})
export class HomeComponent extends AppSmartBaselineComponent implements OnInit {
  public totalDriversCount$: Observable<number>;
  public fuzzySearchResults$: Observable<FuzzySearchResult<Driver>[]>;
  public jumpstartFilterText$: Observable<string>;
  public debounceFilterboxConfig$: Observable<DebounceInputConfig>;
  public driversViewportItemsCount$: Observable<number>;
  public driversCollectionInitialized$: Observable<boolean>;

  private readonly fuzzySearchOptions: FuzzySearchOptions<Driver>;

  public constructor(
    private readonly utils: UtilsService,
    private readonly routerFacade: RouterFacade,
    private readonly fuzzySearchService: FuzzySearchService,

    private readonly driversStore$: Store<fromDriversStore.DriversState>,
    private readonly coreStatusStore$: Store<fromCoreStatusStore.CoreStatusState>,
    private readonly homesStatusesStore$: Store<fromHomeStatusesStore.HomesStatusesState>,
    private readonly configurationsStore$: Store<fromConfigurationStore.ConfigurationsState>
  ) {
    super(coreStatusStore$);

    this.fuzzySearchOptions = FuzzySearchService.spawnOptions<Driver>({ keys: [this.utils.nameof<Driver>('name')] });
  }

  public ngOnInit(): void {
    this
      .wireUpDriversList()
      .wireUpTotalDriversCount()
      .loadOnceInitialFilterTerm()
      .wireUpDriversViewportItemsCount()
      .wireUpDriversCollectionInitialized()
      .wireUpDebounceFilterboxConfigDelay()
      .dispatchCacheCheckAllDriversRequest()
      .dispatchSetCoreStatus({
        tabTitle: 'Home',
        enlargedCoreSectionMode: false,

        header: { text: 'Drivers', showBackButton: false }
      });
  }

  public filterTermChanged($event: DebouncedInputChangedEventArgs): void {
    this.homesStatusesStore$.dispatch(fromHomeStatusesStore.upsertHomeStatus({
      homeStatus: { driversFilterTerm: `${$event.value}` } //deepclone
    }));
  }

  public driverClicked($event: DriverClickedEventArgs): void {
    this.routerFacade.navigateTo({ path: ['/drivers/', $event.info.id] });
  }

  private wireUpDriversCollectionInitialized(): HomeComponent {
    this.driversCollectionInitialized$ = this.driversStore$.pipe(
      select(fromDriversStore.selectTimestamp),
      map(timestamp => timestamp > 0),
      takeUntil(this.ngUnsubscribe$)
    );

    return this;
  }

  private loadOnceInitialFilterTerm(): HomeComponent {
    this.jumpstartFilterText$ = this.homesStatusesStore$.pipe(
      select(fromHomeStatusesStore.selectDriversFilterTerm),
      take(1), //0
    );

    return this;

    //0 we only want to grab the initial value nothing else
  }

  private dispatchCacheCheckAllDriversRequest(): HomeComponent {
    this.driversStore$.dispatch(fromDriversStore.cacheCheckAllDrivers());

    return this;
  }

  private wireUpDriversList(): HomeComponent {
    const allDrivers$ = this.driversStore$.pipe(
      select(fromDriversStore.selectAll),
      takeUntil(this.ngUnsubscribe$)
    );

    const searchTerm$ = this.homesStatusesStore$.pipe(
      select(fromHomeStatusesStore.selectDriversFilterTerm),
      takeUntil(this.ngUnsubscribe$)
    );

    this.fuzzySearchResults$ = combineLatest([searchTerm$, allDrivers$]).pipe(
      map(
        ([searchTerm, allDrivers]) => ({
          searchTerm: searchTerm,
          allDriversSorted: allDrivers
            .map(x => ({ //pure
              entry: x,
              nameUppercased: this.utils.deburr(x.name).toUpperCase()
            }))
            .sort((a, b) => a.nameUppercased.localeCompare(b.nameUppercased))
            .map(x => x.entry)
        })
      ),
      map(({ searchTerm, allDriversSorted }) => this.fuzzySearchService.search(allDriversSorted, searchTerm, this.fuzzySearchOptions)),
      takeUntil(this.ngUnsubscribe$)
    );

    return this;
  }

  private wireUpTotalDriversCount(): HomeComponent {
    this.totalDriversCount$ = this.driversStore$.pipe(
      select(fromDriversStore.selectTotal),
      takeUntil(this.ngUnsubscribe$)
    );

    return this;
  }

  private wireUpDriversViewportItemsCount(): HomeComponent {
    this.driversViewportItemsCount$ = this.configurationsStore$.pipe(
      select(fromConfigurationStore.selectDriversViewportItemsCount),
      filter(x => !!x), //vital
      takeUntil(this.ngUnsubscribe$)
    );

    return this;
  }

  private wireUpDebounceFilterboxConfigDelay(): HomeComponent {
    this.debounceFilterboxConfig$ = this.configurationsStore$.pipe(
      select(fromConfigurationStore.selectDefaultConfigurationDebounceFilterboxConfigDelay),
      filter(x => !!x), //vital
      map(x => ({ delay: x } as DebounceInputConfig)),
      takeUntil(this.ngUnsubscribe$)
    );

    return this;
  }
}

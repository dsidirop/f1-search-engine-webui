import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MessageBoardComponent } from './components/smarties';
import { MessageBoardGuard } from './guards/message-board.guard';

const messageBoardRouting: ModuleWithProviders = RouterModule.forChild([
  {
    path: '',
    pathMatch: 'full',
    component: MessageBoardComponent,
    canActivate: [MessageBoardGuard]
  }
]);

@NgModule({
  imports: [messageBoardRouting],
  exports: [RouterModule]
})

export class MessageBoardRoutingModule { }

import { ScrollingModule } from '@angular/cdk/scrolling';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '@app-shared';
import { MessageBoardComponent } from './components/smarties';
import { MessageBoardRoutingModule } from './message-board-routing.module';

@NgModule({
  imports: [
    FormsModule,
    SharedModule,
    ScrollingModule,
    MessageBoardRoutingModule
  ],
  declarations: [
    MessageBoardComponent
  ]
})

export class MessageBoardModule { }

import { Component, OnInit } from '@angular/core';
import { AppSmartBaselineComponent } from '@app-shared/components/app-smart-baseline.component';
import * as fromCoreStatus from '@app-shared/store/core-status';
import { Shenanigans } from '@app-shared/store/core-status';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-message-board',
  templateUrl: './message-board.component.html',
  styleUrls: ['./message-board.component.scss'],
  providers: []
})
export class MessageBoardComponent extends AppSmartBaselineComponent implements OnInit {

  public shenanigans$: Observable<Shenanigans>;

  public constructor(private readonly coreStatusStore$: Store<fromCoreStatus.CoreStatusState>) {
    super(coreStatusStore$);
  }

  public ngOnInit(): void {
    this.initOnceCoreStatusShenanigans();
  }

  private initOnceCoreStatusShenanigans(): void {
    this.shenanigans$ = this.coreStatusStore$.pipe(
      select(fromCoreStatus.selectShenanigans),
      take(1)
    );
  }
}

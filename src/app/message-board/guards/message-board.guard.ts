import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import * as fromCoreStatusStore from '@app-shared/store/core-status';
import { RouterFacade } from '@app-shared/store/router/router.facade';
import { select, Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class MessageBoardGuard implements CanActivate {

  public constructor(
    private readonly routerFacade: RouterFacade,
    private readonly coreStatusStore$: Store<fromCoreStatusStore.CoreStatus>
  ) {
  }

  public canActivate(/*_: ActivatedRouteSnapshot, __: RouterStateSnapshot*/): Observable<boolean> {
    return this.coreStatusStore$.pipe(
      select(fromCoreStatusStore.selectShenanigans),
      switchMap(shenanigans => {
        const canActivate = !!shenanigans?.message; //0
        if (!canActivate) {
          this.routerFacade.navigateTo({ path: ['/'] });
        }
        return of(canActivate);
      })
    );

    //0 if there is no shenanigans message to display that is a tellsign that the user tries to
    //  access the error page directly in which case there is no point to allow it   we simply
    //  direct the user back to the homepage instead
  }
}

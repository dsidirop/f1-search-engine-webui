import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpCancelService } from '@app-shared/services';
import { Observable } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

//                         stackoverflow.com/a/49797920/863651
//
// we want to cancel any and all outstanding http cals when navigating away from our views

@Injectable()
export class HttpCancelInterceptor implements HttpInterceptor {

    public constructor(private readonly httpCancelService: HttpCancelService) {
    }

    public intercept = <T>(request: HttpRequest<T>, next: HttpHandler): Observable<HttpEvent<T>> =>
        next
            .handle(request)
            .pipe(takeUntil(this.httpCancelService.onCancelPendingRequests()))
}

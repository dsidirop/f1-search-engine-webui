import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadingAnimationDancingDotsComponent } from './loading-animation-dancing-dots.component';

describe('LoadingAnimationDancingDotsComponent', () => {
  let component: LoadingAnimationDancingDotsComponent;
  let fixture: ComponentFixture<LoadingAnimationDancingDotsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LoadingAnimationDancingDotsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadingAnimationDancingDotsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

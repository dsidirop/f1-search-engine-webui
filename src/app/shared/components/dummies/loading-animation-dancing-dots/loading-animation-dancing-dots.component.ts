import { Component } from '@angular/core';
import { AppBaselineComponent } from '@app-shared/components/app-baseline.component';

@Component({
  selector: 'app-loading-anim-dancing-dots',
  templateUrl: './loading-animation-dancing-dots.component.html',
  styleUrls: ['./loading-animation-dancing-dots.component.scss']
})
export class LoadingAnimationDancingDotsComponent extends AppBaselineComponent {
  public constructor() {
    super();
  }
}

import { ColumnSpecs } from './column-specs.model';

export interface GridConfig<TEColumnId> {
  columnsSpecs: ColumnSpecs<TEColumnId, string | number | Date | unknown, string | number | Date | unknown>[];
}

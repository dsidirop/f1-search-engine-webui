export interface ColumnSpecs<
  TEColumnIds,
  TColumnType extends string | number | Date | unknown,
  TColumnTypeForSorting extends string | number | Date | unknown
  > {
  columnId: TEColumnIds;
  valuePath: string;
  valuePathForSorting: string; //for dates

  sortingFn: (a: TColumnTypeForSorting, b: TColumnTypeForSorting) => number
  filteringFn: (a: TColumnType, b: TColumnType) => boolean
}

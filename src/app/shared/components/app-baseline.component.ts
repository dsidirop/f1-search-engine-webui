import { Component, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';

// intertech.com/Blog/angular-best-practice-unsubscribing-rxjs-observables/

@Component({
  template: 'N/A'
})
export /*abstract dont*/ class AppBaselineComponent implements OnDestroy {
  protected readonly ngUnsubscribe$ = new Subject<void>();

  public ngOnDestroy(): void {
    this.ngUnsubscribe$.next();
    this.ngUnsubscribe$.complete();
  }
}

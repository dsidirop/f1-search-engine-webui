import { Component } from '@angular/core';
import * as fromCoreStatusStore from '@app-shared/store/core-status';
import { CoreStatus } from '@app-shared/store/core-status';
import { Store } from '@ngrx/store';
import { AppBaselineComponent } from './app-baseline.component';

@Component({ template: 'N/A' })
export /*abstract dont*/ class AppSmartBaselineComponent extends AppBaselineComponent {

  public constructor(private readonly _coreStatusStore$: Store<fromCoreStatusStore.CoreStatusState>) {
    super();
  }

  public dispatchSetCoreStatus(newCoreStatus: Partial<CoreStatus>): void {
    this._coreStatusStore$.dispatch(fromCoreStatusStore.upsertCoreStatus({ coreStatus: newCoreStatus }));
  }
}

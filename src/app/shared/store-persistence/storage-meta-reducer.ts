import { LocalStorageService } from '@app-shared/services/local-storage.service';
import { Action, ActionReducer } from '@ngrx/store';
import { merge as _merge, pick as _pick } from 'lodash';

export function storageMetaReducer<S, A extends Action = Action>(
  saveKeys: string[],
  localStorageKey: string,
  storageService: LocalStorageService
): (reducer: ActionReducer<S, A>) => (S, A) => S {
  let onInit = true; // after load/refresh
  return function (reducer: ActionReducer<S, A>): (S, A) => S {
    return function (state: S, action: A): S {
      const nextState = reducer(state, action); // get the next state

      if (onInit) { // init the application state
        onInit = false;
        const savedState = storageService.getSavedState(localStorageKey);
        return _merge(nextState, savedState);
      }

      const stateToSave = _pick(nextState, saveKeys); // save the next state to the application storage
      storageService.setSavedState(stateToSave, localStorageKey);

      return nextState;
    };
  };
}

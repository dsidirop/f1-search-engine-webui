import { InjectionToken } from '@angular/core';
import { StoreConfig } from '@ngrx/store/src/store_module';
import * as fromReducer from '@app-shared/store/drivers/drivers.reducer';
import * as fromActions from '@app-shared/store/drivers/drivers.actions';

export const DRIVERS_METAREDUCER_TOKEN = new InjectionToken<StoreConfig<
    fromReducer.DriversState,
    fromActions.ActionsTypeGroup
>>('DriversConfigToken');

export const DRIVERS_STORAGE_KEYS = new InjectionToken<keyof fromReducer.DriversState>('DriversStorageKeys');
export const DRIVERS_LOCAL_STORAGE_KEY = new InjectionToken<string>('DriversStorage');

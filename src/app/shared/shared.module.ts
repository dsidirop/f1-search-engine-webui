import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { QuicklinkModule } from 'ngx-quicklink';
import { AppBaselineComponent, AppSmartBaselineComponent, LoadingAnimationDancingDotsComponent } from './components';
import { AppBaselineDirective, DebounceInputDirective, FocusOnLoadDirective, ViewModeDirective } from './directives';
import { FuzzySearchResultChunkSplitterPipe } from './pipes/fuzzy-search-result-chunk-splitter.pipe';
import { HumanAgePipe } from './pipes/human-age.pipe';
import './prototypes/ngrx-devtools'; //vital for ngrx devtools
import { SharedStoreModule } from './shared-store.module';

@NgModule({
  declarations: [
    //components
    AppBaselineComponent,
    AppSmartBaselineComponent,
    LoadingAnimationDancingDotsComponent,
    //directives
    ViewModeDirective,
    AppBaselineDirective,
    FocusOnLoadDirective,
    DebounceInputDirective,
    //pipes
    HumanAgePipe,
    FuzzySearchResultChunkSplitterPipe
  ],
  imports: [
    CommonModule,
    HttpClientModule
  ],
  exports: [
    //modules
    CommonModule,
    QuicklinkModule,
    SharedStoreModule,
    //components
    AppBaselineComponent,
    AppSmartBaselineComponent,
    LoadingAnimationDancingDotsComponent,
    //directives
    ViewModeDirective,
    AppBaselineDirective,
    FocusOnLoadDirective,
    DebounceInputDirective,
    //pipes
    HumanAgePipe,
    FuzzySearchResultChunkSplitterPipe
    //HttpClientModule   //dont   https://github.com/angular/angular/issues/35255
  ]
})

export class SharedModule { }

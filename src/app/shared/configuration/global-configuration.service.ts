import { Injectable } from '@angular/core';
import { environment } from '@app-env/environment';
import { GlobalConfiguration } from '@app-shared/models';
import { UtilsService } from '@app-shared/services/utils.service';

// medium.com/@seangwright/the-best-way-to-use-angulars-environment-files-a0c098551abc

@Injectable({ providedIn: 'root' })
export class GlobalConfigurationService implements GlobalConfiguration {
  private g: GlobalConfiguration;

  private constructor(private readonly utils: UtilsService) {
    this.g = this.sanitizeConfiguration(environment);
  }

  public get globalConfiguration(): GlobalConfiguration { return this.g; }

  public get backendUrl(): string { return this.g.backendUrl; }
  public get production(): boolean { return this.g.production; }
  public get ajaxRetries(): number { return this.g.ajaxRetries; }
  public get searchboxDebounceTime(): number { return this.g.searchboxDebounceTime; }
  public get driversViewportItemsCount(): number { return this.g.driversViewportItemsCount; }
  public get gridColumnTweakDebounceTime(): number { return this.g.gridColumnTweakDebounceTime; }
  public get racingHistoryViewportItemsCount(): number { return this.g.racingHistoryViewportItemsCount; }
  public get cleanFetchAllDriversThrottlingTime(): number { return this.g.cleanFetchAllDriversThrottlingTime; }
  public get fetchDriverRacingHistoryThrottlingTime(): number { return this.g.fetchDriverRacingHistoryThrottlingTime; }

  public updateAll(newConfiguration: GlobalConfiguration): void {
    this.g = this.sanitizeConfiguration(newConfiguration);
  }

  private sanitizeConfiguration(newConfiguration: GlobalConfiguration): GlobalConfiguration {
    const {
      production,
      backendUrl,
      ajaxRetries,
      searchboxDebounceTime,
      driversViewportItemsCount,
      gridColumnTweakDebounceTime,
      racingHistoryViewportItemsCount,
      cleanFetchAllDriversThrottlingTime,
      fetchDriverRacingHistoryThrottlingTime
    } = newConfiguration;

    const g = {} as GlobalConfiguration;
    g.backendUrl = this.utils.ensurePostfix(backendUrl, '/'); //0
    g.production = production ?? true;
    g.ajaxRetries = ajaxRetries ?? 1;
    g.searchboxDebounceTime = searchboxDebounceTime ?? 250;
    g.driversViewportItemsCount = driversViewportItemsCount ?? 10;
    g.gridColumnTweakDebounceTime = gridColumnTweakDebounceTime ?? 200;
    g.racingHistoryViewportItemsCount = racingHistoryViewportItemsCount ?? 20;
    g.cleanFetchAllDriversThrottlingTime = cleanFetchAllDriversThrottlingTime ?? 1000 * 60 * 60 * 24; //2 24hours
    g.fetchDriverRacingHistoryThrottlingTime = fetchDriverRacingHistoryThrottlingTime ?? 1000 * 60 * 60 * 24; //2 24hours

    return g;

    //0 we append a trailing slash to play it safe just in case
    //1 we refetch all drivers and their respective race-performance once every twenty four hours
    //  it makes sense considering
    //
    //  - new drivers come about mainly at the very beginning of a new f1 season
    //  - their most recent race-performances on f1 take a few hours to be included in the db
  }
}

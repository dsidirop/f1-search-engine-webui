import { Injectable } from '@angular/core';
import { UtilsService } from '@app-shared/services/utils.service';

// stackoverflow.com/a/55278118/863651

@Injectable({ providedIn: 'root' })
export class ScrollbarWidthService {
  private scrollBarWidth: number = null;

  public constructor(private readonly utilsService: UtilsService) {
  }

  get width(): number {
    return this.scrollBarWidth = this.scrollBarWidth ?? this.measureScrollbarWidth();
  }

  private measureScrollbarWidth(): number {
    if (!this.utilsService.isBrowser) //server side rendering
      return 18; //standard size

    let scrollbox = null;
    try {
      scrollbox = document.createElement('div'); // Add temporary box to wrapper
      scrollbox.style.overflow = 'scroll'; // Make box scrollable
      document.body.appendChild(scrollbox); // Append box to document
      const scrollBarWidth = scrollbox.offsetWidth - scrollbox.clientWidth; // Measure inner width of box

      return scrollBarWidth;
    } finally {
      if (scrollbox) {
        document.body.removeChild(scrollbox); // Remove box
      }
    }
  }
}

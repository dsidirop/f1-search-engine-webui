import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GlobalConfigurationService } from '@app-shared/configuration';
import { ResourceNotFoundException, UnexpectedHttpErrorException } from '@app-shared/exceptions';
import { concat, defer, EMPTY, from, Observable, throwError } from 'rxjs';
import { catchError, map, mergeMap, retry } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class ApiBaseService {

  public constructor(
    private readonly httpClient: HttpClient,
    private readonly globalConfigurationService: GlobalConfigurationService
  ) {
  }

  private backendUrl(): string { return this.globalConfigurationService.backendUrl; } //    we dont cache these two on purpose
  private ajaxRetries(): number { return this.globalConfigurationService.ajaxRetries; } //  if we do we will break the autoupdate mechanism

  public get<T>(path: string): Observable<T> {
    return this.httpClient
      .get(`${this.backendUrl()}${path}`, this.spawnHeaders())
      .pipe(
        retry(this.ajaxRetries()),
        map(x => x as T),
        catchError(this.genericErrorHandler)
      );
  }

  public put<T>(path: string, body: unknown = {}): Observable<T> {
    return this.httpClient
      .put(`${this.backendUrl()}${path}`, JSON.stringify(body), this.spawnHeaders())
      .pipe(
        retry(this.ajaxRetries()),
        map(x => x as T),
        catchError(this.genericErrorHandler)
      );
  }

  public post<T>(path: string, body: unknown = {}): Observable<T> {
    return this.httpClient
      .post(`${this.backendUrl()}${path}`, JSON.stringify(body), this.spawnHeaders())
      .pipe(
        retry(this.ajaxRetries()),
        map(x => x as T),
        catchError(this.genericErrorHandler)
      );
  }

  public delete<T>(path: string): Observable<T> {
    return this.httpClient
      .delete(`${this.backendUrl()}${path}`, this.spawnHeaders())
      .pipe(
        retry(this.ajaxRetries()),
        map(x => x as T),
        catchError(this.genericErrorHandler)
      );
  }

  private spawnHeaders(): { headers: HttpHeaders } {
    return { headers: new HttpHeaders(ApiBaseService.HeadersConfig) };
  }

  private genericErrorHandler(error: HttpErrorResponse | unknown): Observable<never> {
    const asHttpErrorResponse = error as HttpErrorResponse;
    if (!asHttpErrorResponse) {
      return throwError(new UnexpectedHttpErrorException(`[AB.GEH01] Unintelligible Error: '${JSON.stringify(error)}'`));
    }

    if (asHttpErrorResponse.status === 404) {
      return throwError(new ResourceNotFoundException(asHttpErrorResponse.message)); //special handling
    }

    return throwError(
      asHttpErrorResponse.error instanceof ErrorEvent
        ? asHttpErrorResponse.error?.message
        : `Error Code: ${asHttpErrorResponse.status}\nMessage: ${asHttpErrorResponse.message}`
    );
  }

  public getPaginatedBatches<TPageBatch>( //0
    urlPagingFormatter: (pageIndex: number, desiredPageSize: number) => string,
    shouldContinuePredicate: (receivedPageBatch: TPageBatch, desiredPageSize: number, pageIndex: number) => boolean,
    pageSize = 50,
    pageIndex = 0
  ): Observable<TPageBatch> {
    return defer(() => this.fetchPage<TPageBatch>(urlPagingFormatter, pageSize, pageIndex)).pipe(
      mergeMap(batchPair => {
        const batch$ = from([batchPair?.batch]);
        const next$ = (
          shouldContinuePredicate(batchPair.batch, pageSize, batchPair.nextPage)
            ? this.getPaginatedBatches(urlPagingFormatter, shouldContinuePredicate, pageSize, batchPair.nextPage)
            : EMPTY
        ) as Observable<TPageBatch>;
        return concat(batch$, next$);
      })
    );

    //0 inspired by   stackoverflow.com/a/35494766/863651
  }

  public fetchPage<TResults>( //0
    urlPagingFormatter: (pageIndex: number, desiredPageSize: number) => string,
    pageSize = 1000,
    pageIndex = 0
  ): Observable<{ batch: TResults, nextPage: number }> {
    return this
      .get<TResults>(urlPagingFormatter(pageIndex, pageSize))
      .pipe(
        map(batch => ({
          batch: batch,
          nextPage: pageIndex + 1
        }))
      );

    //0 inspired by   stackoverflow.com/a/35494766/863651
  }

  private static readonly HeadersConfig = {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
  };
}

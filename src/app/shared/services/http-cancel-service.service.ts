import { Injectable } from '@angular/core'
import { Subject, Observable } from 'rxjs'

@Injectable({ providedIn: 'root' })
export class HttpCancelService {
  private readonly cancelPendingRequests$ = new Subject<void>();

  public cancelPendingRequests = (): void => this.cancelPendingRequests$.next();
  public onCancelPendingRequests = (): Observable<void> => this.cancelPendingRequests$.asObservable();
}

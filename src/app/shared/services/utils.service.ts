/* eslint-disable @typescript-eslint/no-unused-vars */
import { isPlatformBrowser } from '@angular/common';
import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { ESortMode } from '@app-shared/models/enums';
import compareAsc from 'date-fns/compareAsc';
import differenceInYears from 'date-fns/differenceInYears';
import format from 'date-fns/format';
import parse from 'date-fns/parse';
import { cloneDeep as _cloneDeep, curry as _curry, deburr as _deburr, endsWith as _endsWith, flow as _flow, trim as _trim, upperCase as _upperCase } from 'lodash';
import { Observable, Subscriber } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class UtilsService {
  public readonly isBrowser: boolean;

  public constructor(@Inject(PLATFORM_ID) platformId: string) {
    this.isBrowser = isPlatformBrowser(platformId);
  }

  public differenceInYears = (date1: Date | number, date2: Date | number): number => differenceInYears(date1, date2);

  public observableFromFunction$ = <T>(factory: () => T): Observable<T> => { //0
    return Observable.create((observer: Subscriber<T>) => {
      try {
        observer.next(factory());
      } catch (error) {
        observer.error(error);
      } finally {
        observer.complete();
      }
    });

    //0 stackoverflow.com/a/45114028/863651
  }

  public tryParseDate = (date: string, format: string): Date => {
    try {
      return parse(date, format, new Date());
    } catch (err) {
      return null;
    }
  }

  public nameof = <T>(key: keyof T, _instance?: T): keyof T => key;
  public compareDates = (a: Date | number, b: Date | number): number => compareAsc(a, b);
  public stringifyDateLocalFormat = (date: Date | number): string => format(date, 'iii MMM dd, yyyy');

  public compareAsNumbers = (a: number | string, b: number | string): number => {
    if (a === b) //pointerwise
      return 0;

    if (a === null || b === null)
      return a === null ? -1 : +1;

    return (+a) - (+b);
  }

  public numbersAreEqual = (a: number | string, b: number | string): boolean =>
    this.compareAsNumbers(a, b) === 0;

  public stringContainsWithDeburrAndIgnoreCase = (input: string, substring: string): boolean =>
    input === substring
    || this.f.stringContains(
      this.f.stringDeburrAndUpperCase(input)
    )(
      this.f.stringDeburrAndUpperCase(substring)
    );

  public compareStringsDeburrAndIgnoreCase = (a: string, b: string): number => {
    if (a === b)
      return 0; //pointerwise

    if (a === null || b === null)
      return a === null ? -1 : +1;

    return this.f.stringLocaleCompare(
      this.f.stringDeburrAndUpperCase(a)
    )(
      this.f.stringDeburrAndUpperCase(b)
    );
  }

  public deburr = (input: string): string => _deburr(input);
  public deepClone = <T>(value: T): T => _cloneDeep(value)
  public ensurePostfix = (input: string, postfix: string): string =>
    this.f.stringEnsurePostfix(_trim(input), postfix);

  public getNextSortMode = (currentSortMode: ESortMode): ESortMode => {
    switch (currentSortMode) {
      case null:
      case undefined:
      case ESortMode.None: return ESortMode.Descending;
      case ESortMode.Ascending: return ESortMode.None;
      case ESortMode.Descending: return ESortMode.Ascending;
      default: throw new Error(`[US.GNSM01] [BUG] Unknown Sort Mode value '${currentSortMode}'`);
    }
  }

  private readonly f = {
    stringContains: _curry((x: string, y: string) => x.indexOf(y) !== -1),
    stringEnsurePostfix: _flow(
      (s: string, p: string) => ({ e: _endsWith(s, p), s, p }),
      ({ e, s, p }: { e: boolean, s: string, p: string }) => (e ? s : s + p)
    ),
    stringLocaleCompare: _curry((x: string, y: string) => x.localeCompare(y)),
    stringDeburrAndUpperCase: _flow(_deburr, _upperCase, _trim)
  };
}

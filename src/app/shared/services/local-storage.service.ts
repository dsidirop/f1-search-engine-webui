import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class LocalStorageService {
  public getSavedState(localStorageKey: string): unknown {
    return JSON.parse(localStorage.getItem(localStorageKey));
  }

  public setSavedState(state: unknown, localStorageKey: string): void {
    localStorage.setItem(localStorageKey, JSON.stringify(state));
  }
}

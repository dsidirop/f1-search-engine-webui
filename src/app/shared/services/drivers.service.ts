import { Injectable } from '@angular/core';
import { DriverNotFoundException } from '@app-shared/exceptions/driver-not-found.exception';
import { MapperService } from '@app-shared/mapper/mapper.service';
import { DriverRacingHistoryEntry } from '@app-shared/models/driver-racing-history-entry.model';
import { AllDriversInfosRaw, DriverInfoRaw, RaceRaw } from '@app-shared/models/raw';
import { DriverRacingHistoryRaw } from '@app-shared/models/raw/driver-racing-history-data-raw/driver-racing-history-raw.model';
import { Driver } from '@app-shared/store/drivers';
import { isNil as _isNil } from 'lodash';
import { Observable, of, throwError } from 'rxjs';
import { map, reduce, switchMap } from 'rxjs/operators';
import { ApiBaseService } from './api-base.service';

@Injectable({ providedIn: 'root' })
export class DriversService {
  public constructor(
    private readonly api: ApiBaseService,
    private readonly mapper: MapperService
  ) {
  }

  public getDrivers(
    max: number = null,
    pageSize = 1000,
    startingPageIndex = 0
  ): Observable<Driver[]> {
    const callbacks = (function (max: number) {
      let totalEntriesRetrievedCount = 0;

      const shouldContinuePredicate = (receivedBatch: AllDriversInfosRaw, desiredBatchSize: number) => {
        const currentBatchSize = receivedBatch?.MRData?.DriverTable.Drivers.length;
        totalEntriesRetrievedCount += currentBatchSize;
        return currentBatchSize === desiredBatchSize && (_isNil(max) || max >= totalEntriesRetrievedCount);
      };

      const urlPagingFormatter = (pageIndex: number, desiredBatchSize: number) => {
        const properBatchSize = _isNil(max)
          ? desiredBatchSize
          : Math.min(desiredBatchSize, max - totalEntriesRetrievedCount);

        return `drivers.json?offset=${pageIndex * desiredBatchSize}&limit=${properBatchSize}`;
      };

      return { urlPagingFormatter, shouldContinuePredicate };
    })(max);

    return this.api
      .getPaginatedBatches<AllDriversInfosRaw>(
        callbacks.urlPagingFormatter,
        callbacks.shouldContinuePredicate,
        pageSize,
        startingPageIndex
      )
      .pipe(
        map(x => this.mapper.converter1<AllDriversInfosRaw, Driver>(x)),
        reduce((acc, cur) => acc.concat(cur), [] as Driver[])
      );
  }

  public getRacingHistory(
    driverId: string,
    max: number = null,
    pageSize = 1000,
    startingPageIndex = 0
  ): Observable<DriverRacingHistoryEntry[]> {
    const callbacks = (function (max: number) {
      let totalEntriesRetrievedCount = 0;

      const shouldContinuePredicate = (receivedBatch: DriverRacingHistoryRaw, desiredBatchSize: number) => {
        const currentBatchSize = receivedBatch?.MRData?.RaceTable?.Races?.length;
        totalEntriesRetrievedCount += currentBatchSize;
        return currentBatchSize === desiredBatchSize && (_isNil(max) || max >= totalEntriesRetrievedCount);
      };

      const urlPagingFormatter = (pageIndex: number, desiredBatchSize: number) => {
        const properBatchSize = _isNil(max)
          ? desiredBatchSize
          : Math.min(desiredBatchSize, max - totalEntriesRetrievedCount);

        return `drivers/${driverId}/results.json?offset=${pageIndex * desiredBatchSize}&limit=${properBatchSize}`;
      };

      return { urlPagingFormatter, shouldContinuePredicate };
    })(max);

    return this.api
      .getPaginatedBatches<DriverRacingHistoryRaw>(
        callbacks.urlPagingFormatter,
        callbacks.shouldContinuePredicate,
        pageSize,
        startingPageIndex
      )
      .pipe(
        map(x => this.mapper.converter2<RaceRaw[], DriverRacingHistoryEntry[]>(x?.MRData?.RaceTable?.Races)),
        reduce((acc, cur) => acc.concat(cur), [] as DriverRacingHistoryEntry[])
      );
  }

  public getSingleDriverInfo(driverId: string): Observable<Driver> {
    return this
      .api
      .get<AllDriversInfosRaw>(`drivers/${driverId}.json`) //0
      .pipe(
        switchMap(
          x => {
            const driver = this.mapper.converter3<DriverInfoRaw, Driver>(x?.MRData?.DriverTable?.Drivers[0]);
            return driver
              ? of(driver)
              : throwError(new DriverNotFoundException(driverId));
          }
        )
      );

    //0 strangely enough when asking for the info of a specific driver the json returned has
    //  the exact same structure as when requesting the infos of all drivers en masse
  }
}

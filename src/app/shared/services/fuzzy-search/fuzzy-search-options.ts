import FuseOptions = Fuse.FuseOptions;

export type FuzzySearchOptions<T> = FuseOptions<T>

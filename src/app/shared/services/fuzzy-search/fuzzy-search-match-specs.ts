export interface FuzzySearchMatchSpecs {
    key: string;
    value: string;
    indices: number[][]; // [[ 0, 4 ]]  only two numbers
}

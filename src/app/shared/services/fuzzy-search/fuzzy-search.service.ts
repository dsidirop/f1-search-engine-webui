import { Injectable } from '@angular/core';
import { UtilsService } from '@app-shared/services/utils.service';
import * as Fuse from 'fuse.js';
import { FuzzySearchOptions } from './fuzzy-search-options';
import { FuzzySearchResult } from './fuzzy-search-result';

// adapted from github.com/maleblond/angular-fusejs

@Injectable({ providedIn: 'root' })
export class FuzzySearchService {

  public constructor(private readonly utils: UtilsService) {
  }

  public search<T>(list: Array<T>, searchTerm: string, options: FuzzySearchOptions<T>): FuzzySearchResult<T>[] {
    list = list ?? [];
    return (
      searchTerm
        ? new Fuse(list, options).search(searchTerm)
        : this.utils.deepClone<Array<T>>(list).map(x => ({ item: x, matches: null }))
    ) as FuzzySearchResult<T>[];
  }

  public static spawnOptions<T>(options: FuzzySearchOptions<T> = {}): FuzzySearchOptions<T> {
    return {
      location: 0,
      distance: 100,
      threshold: 0.4,
      shouldSort: true,
      includeScore: false,
      caseSensitive: false,
      findAllMatches: true,
      useExtendedSearch: false,
      minMatchCharLength: 2,
      ...options,
      includeMatches: true //keep this here
    } as FuzzySearchOptions<T>;
  }
}

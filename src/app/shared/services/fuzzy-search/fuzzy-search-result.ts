export type FuzzySearchResult<T> = Fuse.FuseResultWithMatches<T>

// start:ng42.barrel
export * from './fuzzy-search-match-specs';
export * from './fuzzy-search-options';
export * from './fuzzy-search-result';
export * from './fuzzy-search.service';
// end:ng42.barrel


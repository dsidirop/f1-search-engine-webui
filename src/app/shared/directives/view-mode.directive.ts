import { Directive, HostBinding } from '@angular/core';
import * as fromCoreStatusStore from '@app-shared/store/core-status';
import { select, Store } from '@ngrx/store';
import { takeUntil } from 'rxjs/operators';
import { AppBaselineDirective } from './app-baseline.directive';

@Directive({ selector: '[appViewMode]' })
export class ViewModeDirective extends AppBaselineDirective {

  @HostBinding('class.f1-enlarged-core-section-mode')
  enlargedCoreSectionMode = false;

  public constructor(private readonly coreStatusStore$: Store<fromCoreStatusStore.CoreStatusState>) {
    super();

    this.wireUpCoreStatus();
  }

  private wireUpCoreStatus(): ViewModeDirective {
    this.coreStatusStore$.pipe(
      select(fromCoreStatusStore.selectEnlargedCoreSectionMode),
      takeUntil(this.ngUnsubscribe$)
    ).subscribe(x => this.enlargedCoreSectionMode = x);

    return this;
  }
}

import { AfterViewInit, Directive, ElementRef } from '@angular/core';
import { UtilsService } from '@app-shared/services';
import { AppBaselineDirective } from './app-baseline.directive';

// //github.com/angular/angular/issues/15674#issuecomment-415311593

@Directive({ selector: '[appFocusOnLoad]' })
export class FocusOnLoadDirective extends AppBaselineDirective implements AfterViewInit {

  public constructor(
    private readonly utils: UtilsService,
    private readonly element: ElementRef<HTMLElement>
  ) {
    super();
  }

  public ngAfterViewInit(): void {
    if (!this.utils.isBrowser) {
      return;
    }

    this.element.nativeElement.focus();
  }
}

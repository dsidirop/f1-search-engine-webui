import { Directive, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';

// intertech.com/Blog/angular-best-practice-unsubscribing-rxjs-observables/

@Directive({ selector: 'N/A' })
export /*abstract dont*/ class AppBaselineDirective implements OnDestroy {
  protected readonly ngUnsubscribe$ = new Subject<void>();

  ngOnDestroy(): void {
    this.ngUnsubscribe$.next();
    this.ngUnsubscribe$.complete();
  }
}

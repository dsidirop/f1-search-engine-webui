import { AfterViewInit, Directive, ElementRef, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { fromEvent } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, map, takeUntil } from 'rxjs/operators';
import { AppBaselineDirective } from './app-baseline.directive';

// stackblitz.com/edit/angular-debounce-input-decorator?file=app%2Fdebounce-input.directive.ts
//
// works with input/textarea and contenteditable  usage
//
//     <input (debounceInput)="changeName($event)">
//     <input (debounceInput)="changeName($event)" [debounceInputConfig]="{delay:500, event:"keyup"}">
//
// As you can tell debounceInputConfig is optional with defaults delay set to 300 and event set to keyup

export interface DebounceInputConfig {
  delay?: number;
  event?: string; // keyup keydown keypress
}

export interface DebouncedInputChangedEventArgs {
  value: string;
}

@Directive({ selector: '[appDebounceInput]' })
export class DebounceInputDirective extends AppBaselineDirective implements OnInit, AfterViewInit {

  @Output('debounceInputChanged')
  public value: EventEmitter<DebouncedInputChangedEventArgs> = new EventEmitter<DebouncedInputChangedEventArgs>();

  @Input()
  public debounceInputConfig: DebounceInputConfig = {};

  public constructor(public readonly el: ElementRef) {
    super();
  }

  public ngOnInit(): void {
    this.debounceInputConfig.delay = this.debounceInputConfig.delay ?? 300;
    this.debounceInputConfig.event = this.debounceInputConfig.event ?? 'keyup';
  }

  public ngAfterViewInit(): void {
    fromEvent(this.el.nativeElement, this.debounceInputConfig.event)
      .pipe(
        filter(Boolean), //0
        debounceTime(this.debounceInputConfig.delay),
        map((event: KeyboardEvent) => {
          const targetElement = event.target as HTMLInputElement;
          const isInputableField = targetElement?.nodeName?.toUpperCase() === 'INPUT' || targetElement?.nodeName?.toUpperCase() === 'TEXTAREA';
          const text = (isInputableField ? targetElement.value : targetElement.innerText)?.trim();
          return text;
        }),
        distinctUntilChanged((previous, current) => previous?.toUpperCase() === current?.toUpperCase()),
        takeUntil(this.ngUnsubscribe$)
      )
      .subscribe(text => this.value.emit({ value: text }));

    //0 filter(boolean) ensures we dont pass undefined values
  }
}

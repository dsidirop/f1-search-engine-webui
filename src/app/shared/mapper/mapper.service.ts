import { Injectable } from '@angular/core';
import { DriverRacingHistoryEntry } from '@app-shared/models';
import { CarConstructor } from '@app-shared/models/car-constructor.model';
import { Circuit } from '@app-shared/models/circuit.model';
import { LocationX } from '@app-shared/models/location-x.model';
import { AllDriversInfosRaw, CarConstructorRaw, DriverInfoRaw, LocationRaw, RaceRaw, ResultRaw } from '@app-shared/models/raw';
import { CircuitRaw } from '@app-shared/models/raw/driver-racing-history-data-raw/circuit-raw.model';
import { UtilsService } from '@app-shared/services/utils.service';
import { Driver } from '@app-shared/store/drivers';

@Injectable({ providedIn: 'root' })
export class MapperService {
  public constructor(private readonly utils: UtilsService) {
  }

  public converter1 = <IN extends AllDriversInfosRaw, OUT extends Driver>(allDriversInfosRaw: AllDriversInfosRaw): Driver[] =>
    allDriversInfosRaw
      ?.MRData
      ?.DriverTable
      ?.Drivers
      ?.map(y => this.converter3(y))
    ?? ([] as Driver[]);

  public converter2 = <IN extends RaceRaw[], OUT extends DriverRacingHistoryEntry[]>(raceRaw: RaceRaw[]): DriverRacingHistoryEntry[] => (
    raceRaw
      ?.map(
        race => race
          ?.Results
          ?.map((z: ResultRaw) => {
            const dateTimestamp = this.utils.tryParseDate(race.date, 'yyyy-MM-dd')?.getTime();
            if (!dateTimestamp) {
              throw new Error(`[MS.CVTR2_01] [SAFEGUARD] Invalid date-format '${race.date}'`);
            }

            return ({
              dateTimestamp: dateTimestamp, //0
              circuitSpecs: this.converter4<CircuitRaw, Circuit>(race.Circuit),
              ...this.converter6<ResultRaw, DriverRacingHistoryEntry>(z)
            }) as DriverRacingHistoryEntry;
          })
      )
      ?.reduce<DriverRacingHistoryEntry[]>((acc, current) => acc.concat(current), [])
    ?? ([] as DriverRacingHistoryEntry[])

    //0 must provide a date at all costs because its being used as a key in the ngrx store
  );

  public converter3 = <IN extends DriverInfoRaw, OUT extends Driver>(driverInfoRaw: IN): OUT => {
    if (!driverInfoRaw)
      return null;

    const dateOfBirthTimestamp = this.utils
      .tryParseDate(driverInfoRaw.dateOfBirth, 'yyyy-MM-dd')
      ?.getTime();
    if (!dateOfBirthTimestamp) {
      throw new Error(`[MS.CVTR3_01] [SAFEGUARD] Invalid date-format '${driverInfoRaw.dateOfBirth}'`);
    }

    return {
      id: driverInfoRaw.driverId,
      url: driverInfoRaw.url,
      name: `${driverInfoRaw.givenName} ${driverInfoRaw.familyName}`,
      nationality: driverInfoRaw.nationality,
      dateOfBirthTimestamp: dateOfBirthTimestamp
    } as OUT;
  };

  public converter4 = <IN extends CircuitRaw, OUT extends Circuit>(circuitRaw: IN): OUT => (
    !circuitRaw
      ? null
      : {
        circuitId: circuitRaw.circuitId,

        url: circuitRaw.url,
        location: this.converter5<LocationRaw, LocationX>(circuitRaw.Location),
        circuitName: circuitRaw.circuitName
      } as OUT
  );

  public converter5 = <IN extends LocationRaw, OUT extends LocationX>(locationRaw: IN): OUT => (
    !locationRaw
      ? null
      : {
        lat: locationRaw.lat,
        long: locationRaw.long,
        country: locationRaw.country,
        locality: locationRaw.locality
      } as OUT
  );

  public converter6 = <IN extends ResultRaw, OUT extends DriverRacingHistoryEntry>(resultRaw: IN): OUT => (
    !resultRaw
      ? null
      : {
        //date: ... //set externally
        //circuitSpecs: ... //set externally

        grid: resultRaw.grid,
        laps: resultRaw.laps,
        status: resultRaw.status,
        number: resultRaw.number,
        points: resultRaw.points,
        position: resultRaw.position,
        positionText: resultRaw.positionText,
        carConstructorSpecs: this.converter7<CarConstructorRaw, CarConstructor>(resultRaw.Constructor)
      } as OUT
  );

  public converter7 = <IN extends CarConstructorRaw, OUT extends CarConstructor>(carConstructorRaw: IN): OUT => (
    !carConstructorRaw
      ? null
      : {
        constructorId: carConstructorRaw.constructorId,

        url: carConstructorRaw.url,
        name: carConstructorRaw.name,
        nationality: carConstructorRaw.nationality
      } as OUT
  );
}

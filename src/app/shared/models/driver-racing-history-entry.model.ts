import { CarConstructor } from './car-constructor.model';
import { Circuit } from './circuit.model';

export class DriverRacingHistoryEntry {
    dateTimestamp: number; //0 date=id
    dateHumanFriendly: string; //helper for the UI

    grid: string;
    laps: string;
    status: string;
    number: string;
    points: string; //1
    position: string;
    positionText: string;

    circuitSpecs: Circuit;
    carConstructorSpecs: CarConstructor; // Constructor.constructorName

    //0 its very hard to imagine how there could ever be two f1 races at the exact same date
    //1 points are awarded to drivers and teams exclusively on where they finish in a race
    //  the winner receives 25 points the second place finisher 18 points with 15 12 10 8 6 4 2
    //  and 1 points for positions 3 through 10
}

import { LocationX } from './location-x.model';

export interface Circuit {
    circuitId: string;

    url: string;
    location: LocationX;
    circuitName: string;
}

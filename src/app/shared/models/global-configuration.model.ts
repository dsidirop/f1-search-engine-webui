export interface GlobalConfiguration {
    production?: boolean;

    backendUrl: string; // http://ergast.com/api/f1/
    ajaxRetries?: number;
    searchboxDebounceTime?: number;
    driversViewportItemsCount?: number;
    gridColumnTweakDebounceTime?: number;
    racingHistoryViewportItemsCount?: number;
    cleanFetchAllDriversThrottlingTime?: number;
    fetchDriverRacingHistoryThrottlingTime?: number;
}

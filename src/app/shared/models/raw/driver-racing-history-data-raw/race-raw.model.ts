import { CircuitRaw } from './circuit-raw.model';
import { ResultRaw } from './result-raw.model';

export interface RaceRaw {
    url: string;
    time: string;
    date: string;
    round: string;
    season: string;
    Circuit: CircuitRaw;
    Results: ResultRaw[];
    raceName: string;
}

import { TimeRaw } from './time-raw.model';
import { AverageSpeedRaw } from './average-speed-raw.model';

export interface FastestLapRaw {
    rank: string;
    lap: string;
    Time: TimeRaw;
    AverageSpeed: AverageSpeedRaw;
}

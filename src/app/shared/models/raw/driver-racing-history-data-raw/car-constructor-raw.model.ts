export interface CarConstructorRaw {
    constructorId: string;

    url: string;
    name: string;
    nationality: string;
}

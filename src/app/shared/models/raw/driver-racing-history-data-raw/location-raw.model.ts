export interface LocationRaw {
    lat: string;
    long: string;

    country: string;
    locality: string;
}

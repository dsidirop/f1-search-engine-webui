import { LocationRaw } from './location-raw.model';

export interface CircuitRaw {
    circuitId: string;

    url: string;
    Location: LocationRaw; //keep capitalized
    circuitName: string;
}

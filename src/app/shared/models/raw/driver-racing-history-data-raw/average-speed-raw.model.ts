export interface AverageSpeedRaw {
    units: string;
    speed: string;
}

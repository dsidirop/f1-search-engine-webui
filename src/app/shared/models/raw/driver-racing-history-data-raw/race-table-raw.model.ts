import { RaceRaw } from './race-raw.model';

export interface RaceTableRaw {
    driverId: string;
    Races: RaceRaw[];
}

import { MDriverRacingHistoryDataRaw } from './mdriver-racing-history-data-raw.model';

export interface DriverRacingHistoryRaw {
    MRData: MDriverRacingHistoryDataRaw;
}

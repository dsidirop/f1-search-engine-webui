import { CarConstructorRaw } from './car-constructor-raw.model';
import { DriverRaw } from './driver-raw.model';
import { FastestLapRaw } from './fastest-lap-raw.model';

export interface ResultRaw {
    number: string;
    position: string;
    positionText: string;
    points: string;
    Driver: DriverRaw;
    Constructor: CarConstructorRaw;
    grid: string;
    laps: string;
    status: string;
    FastestLap: FastestLapRaw;
}

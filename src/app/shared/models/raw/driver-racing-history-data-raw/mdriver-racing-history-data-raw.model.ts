import { RaceTableRaw } from './race-table-raw.model';

export interface MDriverRacingHistoryDataRaw {
    xmlns: string;
    series: string;
    url: string;
    limit: string;
    offset: string;
    total: string;
    RaceTable: RaceTableRaw;
}

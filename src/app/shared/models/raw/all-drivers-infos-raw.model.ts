import { DriverInfoRaw } from './driver-info-raw.model';

export interface AllDriversInfosRaw {
    MRData: {
        url: string; //     "http://ergast.com/api/f1/drivers.json"
        xmlns: string; //   "http://ergast.com/mrd/1.4"
        series: string; //  "f1"

        total: string; //   "848"
        limit: string; //   "10"
        offset: string; //  "100"

        DriverTable: {
            Drivers: DriverInfoRaw[];
        };
    };
}

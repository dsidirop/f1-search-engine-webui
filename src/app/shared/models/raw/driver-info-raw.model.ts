export interface DriverInfoRaw {
    driverId: string; // "brabham"

    url: string; // "http://en.wikipedia.org/wiki/David_Brabham"
    givenName: string;
    familyName: string;
    dateOfBirth: string; // "1965-09-05"
    nationality: string; // "Australian"
}


import { ESortMode } from './enums';

export interface ColumnState<T> {
    id: T; //                     columnname
    sortMode: ESortMode; //       sorting
    filterTerm: string; //        filtering
    sortChainIndex: number; //    for orderby->thenby->thenby sorting
}

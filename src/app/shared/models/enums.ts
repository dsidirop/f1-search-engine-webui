export const enum EApp {
  TitlePrefix = 'F1'
}

export const enum EStandardStoreKeys {
  Default = 'default'
}

// due to a glitch between the latest typescript vs angular we cant turn this into a const
// if we turn it into const then we wont be able to use it in the angular templates
export /*const dont*/ enum EDriverRacingHistoryColumnIds {
  Date = 'date',
  Grid = 'grid',
  Laps = 'laps',
  Team = 'team',
  Points = 'points',
  Status = 'status',
  Number = 'number',
  Circuit = 'circuit',
  Position = 'position'
}

// due to a glitch between the latest typescript vs angular we cant turn this into a const
// if we turn it into const then we wont be able to use it in the angular templates
export /*const*/ enum ESortMode {
  Ascending = +1,
  None = 0,
  Descending = -1
}

// the trailing 'x' is there to avoid a conflict with the
// global symbol Location which is provided by the browser

export interface LocationX {
    lat: string;
    long: string;

    country: string;
    locality: string;
}

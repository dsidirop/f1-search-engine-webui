export interface CarConstructor {
    constructorId: string;

    url: string;
    name: string;
    nationality: string;
}

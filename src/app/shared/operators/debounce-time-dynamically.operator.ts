import { MonoTypeOperatorFunction, of } from 'rxjs';
import { debounce, delay } from 'rxjs/operators';

export function debounceTimeDynamically<T>(callback: (x: T) => number): MonoTypeOperatorFunction<T> {
  return debounce(
    payload => of(true).pipe(
      delay(callback(payload))
    )
  )
}

import { isNil } from 'lodash';
import { MonoTypeOperatorFunction } from 'rxjs';
import { filter } from 'rxjs/operators';

export function notNil<T>(): MonoTypeOperatorFunction<T> {
  return source => source.pipe(filter(results => !isNil(results)));
}

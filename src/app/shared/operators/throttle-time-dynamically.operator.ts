import { MonoTypeOperatorFunction, of } from 'rxjs';
import { delay, throttle } from 'rxjs/operators';

export function throttleTimeDynamically<T>(callback: (x: T) => number): MonoTypeOperatorFunction<T> {
  return throttle(
    payload => of(true).pipe(
      delay(callback(payload))
    )
  )
}

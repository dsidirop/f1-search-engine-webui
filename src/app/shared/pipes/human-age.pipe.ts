import { Pipe, PipeTransform } from '@angular/core';
import { UtilsService } from '@app-shared/services/utils.service';
import { Observable, Subscriber } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Pipe({ name: 'appHumanAge', pure: true })
export class HumanAgePipe implements PipeTransform {
  public constructor(private readonly utils: UtilsService) {
  }

  public transform(birthday: Date | number): Observable<string> {
    return new Observable<string>((observer: Subscriber<string>) => {
      try {
        observer.next('' + this.utils.differenceInYears(new Date(), birthday));
      } catch (err) {
        observer.error(err);
      } finally {
        observer.complete();
      }
    }).pipe(
      catchError(err => {
        console.error(err);
        return null as string;
      })
    );
  }
}

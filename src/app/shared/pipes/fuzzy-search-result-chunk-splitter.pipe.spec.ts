import { inject, TestBed } from '@angular/core/testing';
import { FuzzySearchResultChunkSplitterPipe } from './fuzzy-search-result-chunk-splitter.pipe';

describe('FuzzySearchResultChunkSplitterPipe', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: []
    });
  });

  it('create an instance', inject([], () => {
    const pipe = new FuzzySearchResultChunkSplitterPipe();
    expect(pipe).toBeTruthy();
  }));
});

import { Pipe, PipeTransform } from '@angular/core';
import { FuzzySearchMatchSpecs, FuzzySearchResult } from '@app-shared/services';
import { get as _get } from 'lodash';
import { EMPTY, Observable, Subscriber } from 'rxjs';
import { catchError, reduce } from 'rxjs/operators';

@Pipe({ name: 'appFuzzySearchResultChunkSplitter', pure: true })
export class FuzzySearchResultChunkSplitterPipe implements PipeTransform {

  public transform(fuzzySearchResults: FuzzySearchResult<unknown>, keyPropertyPath: string): Observable<SingleComponent[]> {
    return new Observable<SingleComponent[]>((observer: Subscriber<SingleComponent[]>) => {
      try {
        const specificPropertyMatchSpecs = this.healthcheck(observer, fuzzySearchResults, keyPropertyPath);
        if (!specificPropertyMatchSpecs) return; //error or simply guard close

        this.convertToSingleStringComponents(observer, specificPropertyMatchSpecs);
      } catch (err) {
        observer.error(err);
      } finally {
        observer.complete();
      }
    }).pipe(
      reduce((accumulator, value) => accumulator.concat(value), [] as SingleComponent[]),
      catchError(err => {
        console.error(err);
        return EMPTY;
      })
    );
  }

  private healthcheck(
    observer: Subscriber<SingleComponent[]>,
    fuzzySearchResults: FuzzySearchResult<unknown>,
    keyPropertyPath: string
  ): FuzzySearchMatchSpecs {
    keyPropertyPath = keyPropertyPath?.trim();
    if (!keyPropertyPath) throw new Error('[FSRCSP.TRFM01] [BUG] keyPropertyPath is mandatory!');

    const { item, matches: allMatchSpecs } = fuzzySearchResults;
    if (!allMatchSpecs?.length) {
      observer.next([{ start: 0, text: _get(item, keyPropertyPath), isMatch: false }]);
      return;
    }

    const specificPropertyMatchSpecs = (allMatchSpecs as FuzzySearchMatchSpecs[]).filter(
      x => x.key.toUpperCase() === keyPropertyPath.toUpperCase()
    )[0];
    if (!specificPropertyMatchSpecs) {
      throw new Error(`[FSRCSP.TRFM02] [BUG] Path '${keyPropertyPath}' not detected in search-results!`);
    }


    return specificPropertyMatchSpecs;
  }

  private convertToSingleStringComponents(
    observer: Subscriber<SingleComponent[]>,
    specificPropertyMatchSpecs: FuzzySearchMatchSpecs
  ) {
    const { value, indices: matchingChunks } = specificPropertyMatchSpecs;

    const sanitizedChunks = matchingChunks
      .map(chunkIndices => ({ start: chunkIndices[0], end: chunkIndices[1], isMatch: true } as StartAndEndIndicesTuple))
      .filter(chunk => chunk.start < chunk.end); //sanitize just in case

    sanitizedChunks.forEach(
      (currentMatchingChunkSpecs: StartAndEndIndicesTuple, i: number) => {
        const precedingChunkStart = i > 0 ? sanitizedChunks[i - 1].end + 1 : 0;
        const precedingPlainChunkSpecs = (
          currentMatchingChunkSpecs.start > precedingChunkStart //check for room in between
            ? { start: precedingChunkStart, end: currentMatchingChunkSpecs.start - 1, isMatch: false }
            : null
        ) as StartAndEndIndicesTuple;

        const trailingPlainChunkSpecs = ( //special case for the last match
          i === sanitizedChunks.length - 1 && currentMatchingChunkSpecs.end < value.length - 1 //check for room in the end
            ? { start: currentMatchingChunkSpecs.end + 1, end: value.length - 1, isMatch: false }
            : null
        ) as StartAndEndIndicesTuple;

        const newComponentsBatch = [precedingPlainChunkSpecs, currentMatchingChunkSpecs, trailingPlainChunkSpecs]
          .filter(x => !!x)
          .map(x => (
            {
              start: x.start,
              text: value.substring(x.start, x.end + 1),
              isMatch: x.isMatch
            } as SingleComponent
          ));

        observer.next(newComponentsBatch);
      });
  }
}

export interface SingleComponent {
  start: number; //id
  text: string;
  isMatch: boolean;
}

interface StartAndEndIndicesTuple {
  start: number;
  end: number;
  isMatch: boolean;
}

/* eslint-disable @typescript-eslint/no-explicit-any */
import { environment } from '@app-env/environment';

if (!environment.production) {
  (Set.prototype as any).toJSON = function () { //0
    return JSON.parse(JSON.stringify([...this]));
  };
}

//0 we need this tweak for the sake of addressing an issue with ngrx devtools which
//  was causing it to not be able to handle map types properly
//
//  //github.com/ngrx/store-devtools/issues/64#issuecomment-314889392

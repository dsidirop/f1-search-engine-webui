/* eslint-disable @typescript-eslint/no-explicit-any */
import { RouterStateSnapshot } from '@angular/router';
import { RouterStateSerializer } from '@ngrx/router-store';

// //alligator.io/angular/title-updater/

export interface RouterStateTitle {
  url: string;
  title: (string | any);
}

export class CustomRouterStateSerializer implements RouterStateSerializer<RouterStateTitle> {

  serialize(routerState: RouterStateSnapshot): RouterStateTitle {
    let childRoute = routerState.root;
    while (childRoute.firstChild) {
      childRoute = childRoute.firstChild;
    }

    return {
      url: childRoute.data['url'],
      title: childRoute.data['title'] // Use the most specific title
    };
  }

}

/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-empty-interface */
import { environment } from '@app-env/environment';
import { ActionReducer, ActionReducerMap, MetaReducer } from '@ngrx/store';
import { LoggerOptions, storeLogger } from 'ngrx-store-logger';

export interface RootState {
  // placeholder
}

export const rootReducers: ActionReducerMap<RootState> = {
  // placeholder
};

const options: LoggerOptions = {
  collapsed: true
}

export function logger(reducer: ActionReducer<RootState>): (state: any, action: any) => any {
  return storeLogger(options)(reducer); // github.com/btroncone/ngrx-store-logger
}

export const metaReducers: MetaReducer<RootState>[] = environment.production ? [] : [logger];

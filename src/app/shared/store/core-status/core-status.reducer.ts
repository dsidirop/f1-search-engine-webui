import { EStandardStoreKeys } from '@app-shared/models/enums';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { createFeatureSelector, createReducer, createSelector, on } from '@ngrx/store';
import { cloneDeep as _cloneDeep } from 'lodash';
import * as CoreActions from './core-status.actions';
import { CoreStatus } from './core-status.model';

export const coreStatusFeatureKey = 'coreStatus';

export type CoreStatusState = EntityState<CoreStatus>

export const adapter: EntityAdapter<CoreStatus> = createEntityAdapter<CoreStatus>({
  selectId: (core: CoreStatus) => core?.id,
  sortComparer: false
});

export const initialState: CoreStatusState = adapter.getInitialState({
  ids: [EStandardStoreKeys.Default],
  entities: {
    [EStandardStoreKeys.Default]: {
      id: EStandardStoreKeys.Default,
      tabTitle: '',
      enlargedCoreSectionMode: false,

      header: {
        url: '',
        text: '',
        showBackButton: false
      }
    } as CoreStatus
  }
} as CoreStatusState);

const BaseErrorCoreStatus: CoreStatus = {
  id: EStandardStoreKeys.Default,
  tabTitle: null,
  shenanigans: {
    message: null,
    details: null
  },
  enlargedCoreSectionMode: false,

  header: {
    url: null,
    text: null,
    showBackButton: false
  }
};

const coreStatusReducer = createReducer(
  initialState,
  //singles
  on(CoreActions.addCoreStatus, (state, action) => adapter.addOne(action.coreStatus, state)),
  on(CoreActions.deleteCoreStatus, (state, action) => adapter.removeOne(action.id, state)),
  on(CoreActions.updateCoreStatus, (state, action) => adapter.updateOne(action.coreStatus, state)),
  on(CoreActions.upsertCoreStatus, (state, action) => {
    const newCoreStatus = {
      ...action.coreStatus,
      id: action.coreStatus.id ?? EStandardStoreKeys.Default,
      shenanigans: null //ripoff any error data
    } as CoreStatus;

    return adapter.upsertOne(newCoreStatus, state);
  }),
  on(CoreActions.upsertReasonableErrorCoreStatus, (state, action) => { //drivernotfound etc
    const newCoreStatus = _cloneDeep(BaseErrorCoreStatus);
    newCoreStatus.tabTitle = `${action.tabTitle}`;
    newCoreStatus.header.text = `${action.tabTitle}`;
    newCoreStatus.shenanigans = { ...action.shenanigans };

    return adapter.upsertOne(newCoreStatus, state);
  }),
  on(CoreActions.upsertServiceOfflineErrorCoreStatus, (state) => { //serverdown etc
    const newCoreStatus = _cloneDeep(BaseErrorCoreStatus);
    newCoreStatus.tabTitle = 'Service Unavailable';
    newCoreStatus.header.text = 'Service Unavailable';
    newCoreStatus.shenanigans.message = 'Service is currently unavailable - Try again later! If this problem persists contact IT!'

    return adapter.upsertOne(newCoreStatus, state);
  }),
  on(CoreActions.upsertUnexpectedErrorCoreStatus, (state, action) => { //null reference exceptions etc
    const newCoreStatus = _cloneDeep(BaseErrorCoreStatus);
    newCoreStatus.tabTitle = 'Unexpected Error';
    newCoreStatus.header.text = 'Unexpected Error';
    newCoreStatus.shenanigans.message = 'An Unexpected Error has Occurred! Contact IT detailing what you did to cause this!';
    newCoreStatus.shenanigans.details = (action.error as Error)?.message || (action?.error as string) || '(Details N/A)';

    return adapter.upsertOne(newCoreStatus, state);
  }),
  //multiples
  on(CoreActions.addCoreStatuses, (state, action) => adapter.addMany(action.coreStatuses, state)),
  on(CoreActions.loadCoreStatuses, (state, action) => adapter.addAll(action.coreStatuses, state)),
  on(CoreActions.clearCoreStatuses, (state) => adapter.removeAll(state)),
  on(CoreActions.deleteCoreStatuses, (state, action) => adapter.removeMany(action.ids, state)),
  on(CoreActions.upsertCoreStatuses, (state, action) => adapter.upsertMany(action.coreStatuses, state)),
  on(CoreActions.updateCoreStatuses, (state, action) => adapter.updateMany(action.coreStatuses, state))
);

export function reducer(state: CoreStatusState | undefined, action: CoreActions.ActionsTypeGroup): CoreStatusState {
  return coreStatusReducer(state, action);
}

export const selectCoreState = createFeatureSelector<CoreStatusState>(coreStatusFeatureKey);

export const { selectIds, selectEntities, selectAll, selectTotal } = adapter.getSelectors(selectCoreState);

export const selectDefaultCoreStatus = createSelector(
  selectEntities,
  (entities) => entities[EStandardStoreKeys.Default] as CoreStatus
);

export const selectTitle = createSelector(
  selectDefaultCoreStatus,
  defaultCoreState => defaultCoreState?.tabTitle
);

export const selectEnlargedCoreSectionMode = createSelector(
  selectDefaultCoreStatus,
  defaultCoreState => defaultCoreState?.enlargedCoreSectionMode
);

export const selectShenanigans = createSelector(
  selectDefaultCoreStatus,
  defaultCoreState => defaultCoreState?.shenanigans
);

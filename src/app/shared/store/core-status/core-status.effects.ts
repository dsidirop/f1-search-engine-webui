import { Injectable } from '@angular/core';
import * as CoreStatusActions from '@app-shared/store/core-status/core-status.actions';
import * as RouterActions from '@app-shared/store/router/router.actions';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { map } from 'rxjs/operators';

@Injectable()
export class CoreStatusEffects {

  public fetchDriverRequestFailure$ = createEffect(
    () => this.actions$.pipe(
      ofType(
        CoreStatusActions.upsertUnexpectedErrorCoreStatus,
        CoreStatusActions.upsertReasonableErrorCoreStatus,
        CoreStatusActions.upsertServiceOfflineErrorCoreStatus
      ),
      map(() => RouterActions.navigateTo({ path: ['error'] }))
    )
  );

  public constructor(private readonly actions$: Actions) { }
}

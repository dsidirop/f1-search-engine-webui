/* eslint-disable @typescript-eslint/no-explicit-any */
import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';
import { CoreStatusEffects } from './core-status.effects';

describe('CoreStatusEffects', () => {
  let actions$: Observable<any>;
  let effects: CoreStatusEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        CoreStatusEffects,
        provideMockActions(() => actions$)
      ]
    });

    effects = TestBed.get<CoreStatusEffects>(CoreStatusEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });
});

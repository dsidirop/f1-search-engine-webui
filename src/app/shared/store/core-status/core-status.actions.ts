import { Update } from '@ngrx/entity';
import { createAction, props, union } from '@ngrx/store';
import { CoreStatus, Shenanigans } from './core-status.model';

export const addCoreStatus = createAction('[CoreStatus] Add Core Status', props<{ coreStatus: CoreStatus }>());
export const addCoreStatuses = createAction('[CoreStatus] Add Core Statuses', props<{ coreStatuses: CoreStatus[] }>());
export const loadCoreStatuses = createAction('[CoreStatus] Load Core Statuses', props<{ coreStatuses: CoreStatus[] }>());
export const deleteCoreStatus = createAction('[CoreStatus] Delete Core Status', props<{ id: string }>());
export const updateCoreStatus = createAction('[CoreStatus] Update Core Status', props<{ coreStatus: Update<CoreStatus> }>());
export const upsertCoreStatus = createAction('[CoreStatus] Upsert Core Status', props<{ coreStatus: Partial<CoreStatus> }>());
export const clearCoreStatuses = createAction('[CoreStatus] Clear Core Statuses');
export const deleteCoreStatuses = createAction('[CoreStatus] Delete Core Statuses', props<{ ids: string[] }>());
export const upsertCoreStatuses = createAction('[CoreStatus] Upsert Core Statuses', props<{ coreStatuses: CoreStatus[] }>());
export const updateCoreStatuses = createAction('[CoreStatus] Update Core Statuses', props<{ coreStatuses: Update<CoreStatus>[] }>());
export const upsertReasonableErrorCoreStatus = createAction('[CoreStatus] Upsert Sane-Error Core Status', props<{ tabTitle: string, shenanigans: Shenanigans }>());
export const upsertUnexpectedErrorCoreStatus = createAction('[CoreStatus] Upsert Unexpected-Error Core Status', props<{ error: string | Error }>());
export const upsertServiceOfflineErrorCoreStatus = createAction('[CoreStatus] Upsert Service-Offline-Error Core Status');

const actions = union({
    addCoreStatus,
    addCoreStatuses,
    loadCoreStatuses,
    deleteCoreStatus,
    updateCoreStatus,
    upsertCoreStatus,
    clearCoreStatuses,
    deleteCoreStatuses,
    upsertCoreStatuses,
    updateCoreStatuses,
    upsertSaneErrorCoreStatus: upsertReasonableErrorCoreStatus,
    upsertUnexpectedErrorCoreStatus
});

export type ActionsTypeGroup = typeof actions;

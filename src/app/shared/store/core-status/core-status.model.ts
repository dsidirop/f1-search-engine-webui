export interface CoreStatus {
  id?: string; //essentially one core state the default

  tabTitle: string;
  shenanigans: Shenanigans //0 errors
  enlargedCoreSectionMode: boolean;

  header: Partial<{
    url: string;
    text: string;
    showBackButton: boolean;
  }>

  //0 displaying error or redirection message
}

export interface Shenanigans {
  message: string;
  details?: string;
}
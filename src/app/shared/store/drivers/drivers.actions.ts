import { Update } from '@ngrx/entity';
import { createAction, props, union } from '@ngrx/store';
import { Driver } from './driver.model';

export const setInitializedFlag = createAction('[Driver/API] Set Initialized Flag', props<{ newValue: boolean }>());

export const cacheCheckForDriver = createAction('[Driver] Cache-Check for Driver [Request 💬]', props<{ id: string }>());
export const cacheCheckForDriverHit = createAction('[Driver] Cache-Check for Driver [Cache-Hit ✔️(🖴)]');
export const cacheCheckForDriverMiss = createAction('[Driver] Cache-Check for Driver [Cache-Miss ⚡(🖴)]', props<{ id: string }>());

export const fetchDriverRequest = createAction('[Driver/API] Fetch Driver [Request 💬]', props<{ id: string }>());
export const fetchDriverRequestFailure = createAction('[Driver/API] Fetch Driver [Failure ❌]', props<{ error: string | Error }>());
export const fetchDriverRequestSuccess = createAction('[Driver/API] Fetch Driver [Success ✔️]', props<{ driver: Driver }>());

export const cacheCheckAllDrivers = createAction('[Driver] Cache-Check for All Drivers [Request 💬]');
export const cacheCheckAllDriversHit = createAction('[Driver] Cache-Check for All Drivers [Cache-Hit ✔️(🖴)]');
export const cacheCheckAllDriversMiss = createAction('[Driver] Cache-Check for All Drivers [Cache-Miss ⚡(🖴)]');

export const cleanRefetchAllDriversRequest = createAction('[Driver/API] Clean Refetch All Drivers [Request 💬]');
export const cleanRefetchAllDriversRequestFailure = createAction('[Driver/API] Clean Refetch All Drivers [Failure ❌]', props<{ error: string | Error }>());
export const cleanRefetchAllDriversRequestSuccess = createAction('[Driver/API] Clean Refetch All Drivers [Success ✔️]', props<{ drivers: Driver[] }>());

export const addDriver = createAction('[Driver] Add Driver', props<{ driver: Driver }>());
export const addDrivers = createAction('[Driver] Add Drivers', props<{ drivers: Driver[] }>());
export const upsertDriver = createAction('[Driver] Upsert Driver', props<{ driver: Driver }>());
export const clearDrivers = createAction('[Driver] Clear Drivers');
export const updateDriver = createAction('[Driver] Update Driver', props<{ driver: Update<Driver> }>());
export const deleteDriver = createAction('[Driver] Delete Driver', props<{ id: string }>());
export const upsertDrivers = createAction('[Driver] Upsert Drivers', props<{ drivers: Driver[] }>());
export const updateDrivers = createAction('[Driver] Update Drivers', props<{ drivers: Update<Driver>[] }>());
export const deleteDrivers = createAction('[Driver] Delete Drivers', props<{ ids: string[] }>());

const actions = union({
  setInitializedFlag,

  cacheCheckForDriver,
  cacheCheckForDriverHit,
  cacheCheckForDriverMiss,

  fetchDriverRequest,
  fetchDriverRequestFailure,
  fetchDriverRequestSuccess,

  cacheCheckAllDrivers,
  cacheCheckAllDriversHit,
  cacheCheckAllDriversMiss,

  cleanRefetchAllDriversRequestFailure,
  cleanRefetchAllDriversRequestSuccess,

  addDriver,
  addDrivers,
  upsertDriver,
  clearDrivers,
  updateDriver,
  deleteDriver,
  upsertDrivers,
  updateDrivers,
  deleteDrivers

});

export type ActionsTypeGroup = typeof actions;

import { Injectable } from '@angular/core';
import { GlobalConfigurationService } from '@app-shared/configuration';
import { DriverNotFoundException } from '@app-shared/exceptions/driver-not-found.exception';
import { ResourceNotFoundException } from '@app-shared/exceptions/resource-not-found-exception';
import { DriversService } from '@app-shared/services';
import * as CoreStatusActions from '@app-shared/store/core-status/core-status.actions';
import * as FromDriversStore from '@app-shared/store/drivers/drivers.reducer';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { of } from 'rxjs';
import { catchError, concatMap, exhaustMap, map, mergeMap, withLatestFrom } from 'rxjs/operators';
import * as DriversActions from './drivers.actions';

@Injectable()
export class DriversEffects {

  public cacheCheckForDriver$ = createEffect(
    () => this.actions$.pipe(
      ofType(DriversActions.cacheCheckForDriver),
      concatMap(action => of(action).pipe(
        withLatestFrom(this.driversStore$.pipe( //0
          select(FromDriversStore.selectDriverById, action.id),
          map(x => !!x)
        ))
      )),
      map(
        ([action, isCacheHit]) => isCacheHit
          ? DriversActions.cacheCheckForDriverHit()
          : DriversActions.cacheCheckForDriverMiss({ id: action.id }) // => fetchDriverRequest
      )
    )

    //0 immortal technique derived straight from  //ngrx.io/guide/effects
  );

  public fetchDriverRequest$ = createEffect( // + cacheCheckForDriverMiss
    () => this.actions$.pipe(
      ofType(
        DriversActions.fetchDriverRequest,
        DriversActions.cacheCheckForDriverMiss
      ),
      mergeMap(
        action => this.driversService
          .getSingleDriverInfo(action.id)
          .pipe(
            map(driver => DriversActions.fetchDriverRequestSuccess({ driver })),
            catchError(error => of(DriversActions.fetchDriverRequestFailure({ error })))
          )
      )
    )
  );

  public fetchDriverRequestFailure$ = createEffect(
    () => this.actions$.pipe(
      ofType(DriversActions.fetchDriverRequestFailure),
      concatMap(action => {
        const driverNotFoundException = action.error as DriverNotFoundException; //0 due to empty results
        if (driverNotFoundException) {
          return of(CoreStatusActions.upsertReasonableErrorCoreStatus({
            tabTitle: 'Driver Not Found',
            shenanigans: { message: `Driver with id '${driverNotFoundException.driverId}' doesn't exist!` }
          }));
        }

        const serverUnreachableException = action.error as ResourceNotFoundException; //0 due to server being unreachable
        if (serverUnreachableException) {
          return of(CoreStatusActions.upsertServiceOfflineErrorCoreStatus());
        }

        return of(CoreStatusActions.upsertUnexpectedErrorCoreStatus({ error: action.error }));
      })
    )

    //0 in the case of the ergast server if a driver is not found the server doesnt return
    //  a 404notfound response   it instead returns a 200ok with an empty result set
    //
    //  if we do receive a 404response it means that the server is unreachable either because
    //  of a network issue or because the ergast server is offline
  );

  public cacheCheckAllDriversRequest$ = createEffect(
    () => this.actions$.pipe(
      ofType(DriversActions.cacheCheckAllDrivers),
      concatMap(action => of(action).pipe(
        withLatestFrom(this.driversStore$.pipe(
          select(FromDriversStore.selectTimestamp),
          map(timestamp => this.isCacheHit(timestamp))
        ))
      )),
      map(
        ([, isCacheHit]) => isCacheHit
          ? DriversActions.cacheCheckAllDriversHit()
          : DriversActions.cacheCheckAllDriversMiss() // => cleanRefetchAllDriversRequest
      )
    )
  );

  public cleanRefetchAllDriversRequest$ = createEffect(
    () => this.actions$.pipe(
      ofType(
        DriversActions.cacheCheckAllDriversMiss,
        DriversActions.cleanRefetchAllDriversRequest
      ),
      exhaustMap(
        () => this.driversService.getDrivers().pipe(
          map(drivers => DriversActions.cleanRefetchAllDriversRequestSuccess({ drivers })),
          catchError(error => of(DriversActions.cleanRefetchAllDriversRequestFailure({ error })))
        )
      )
    )
  );

  public cleanRefetchAllDriversRequestFailure$ = createEffect(
    () => this.actions$.pipe(
      ofType(DriversActions.cleanRefetchAllDriversRequestFailure),
      concatMap(action => {
        const clearDrivers = DriversActions.clearDrivers();

        const errorCoreStatusAction = action.error as ResourceNotFoundException
          ? CoreStatusActions.upsertServiceOfflineErrorCoreStatus()
          : CoreStatusActions.upsertUnexpectedErrorCoreStatus({ error: action.error });

        return [clearDrivers, errorCoreStatusAction];
      })
    )
  );

  private isCacheHit(timestamp: number): boolean {
    const threshold = this.globalConfigurationService.cleanFetchAllDriversThrottlingTime;
    return (new Date().getTime() - timestamp) <= threshold;
  }

  public constructor(
    //misc
    private readonly actions$: Actions,
    //services
    private readonly driversService: DriversService,
    private readonly globalConfigurationService: GlobalConfigurationService,
    //store
    private readonly driversStore$: Store<FromDriversStore.DriversState>
  ) { }
}

export interface Driver {
    id: string; //   driverId: "brabham",

    url: string; //  "http://en.wikipedia.org/wiki/David_Brabham",
    name: string; // "givenName": "David" / "familyName": "Brabham"
    nationality: string; // "Australian"
    dateOfBirthTimestamp: number; //0

    //0 storing the date of birth as total number of milliseconds from 1970 is better suited for serialization
    //  than the date object itself
}

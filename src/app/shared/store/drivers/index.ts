// start:ng42.barrel
export * from './driver.model';
export * from './drivers.actions';
export * from './drivers.effects';
export * from './drivers.reducer';
// end:ng42.barrel


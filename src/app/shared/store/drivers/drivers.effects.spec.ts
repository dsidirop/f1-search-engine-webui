/* eslint-disable @typescript-eslint/no-explicit-any */
import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';
import { DriversEffects } from './drivers.effects';

describe('DriversEffects', () => {
  let actions$: Observable<any>;
  let effects: DriversEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        DriversEffects,
        provideMockActions(() => actions$)
      ]
    });

    effects = TestBed.get<DriversEffects>(DriversEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });
});

import { createEntityAdapter, Dictionary, EntityAdapter, EntityState } from '@ngrx/entity';
import { createFeatureSelector, createReducer, createSelector, on } from '@ngrx/store';
import { Driver } from './driver.model';
import * as DriversActions from './drivers.actions';

export const driversFeatureKey = 'drivers';

export interface DriversState extends EntityState<Driver> {
  timestamp: number;
}

export const adapter: EntityAdapter<Driver> = createEntityAdapter<Driver>({
  selectId: (driver: Driver) => driver?.id,
  sortComparer: false
});

export const initialState: DriversState = adapter.getInitialState({
  ids: [],
  entities: {},
  timestamp: 0
} as DriversState);

const driversReducer = createReducer(
  initialState,
  //misc
  on(DriversActions.setInitializedFlag, (state, action) => ({
    ...state,
    initialized: action.newValue
  })),
  //singles
  on(DriversActions.addDriver, (state, action) => adapter.addOne(action.driver, state)),
  on(DriversActions.deleteDriver, (state, action) => adapter.removeOne(action.id, state)),
  on(DriversActions.updateDriver, (state, action) => adapter.updateOne(action.driver, state)),
  on(DriversActions.upsertDriver, (state, action) => adapter.upsertOne(action.driver, state)),
  //multiples
  on(DriversActions.addDrivers, (state, action) => adapter.addMany(action.drivers, state)),
  on(DriversActions.clearDrivers, state => ({
    ...adapter.removeAll(state),
    timestamp: 0
  })),
  on(DriversActions.deleteDrivers, (state, action) => adapter.removeMany(action.ids, state)),
  on(DriversActions.updateDrivers, (state, action) => adapter.updateMany(action.drivers, state)),
  on(DriversActions.fetchDriverRequestSuccess, (state, action) => adapter.upsertOne(action.driver, state)),
  on(
    DriversActions.upsertDrivers,
    DriversActions.cleanRefetchAllDriversRequestSuccess,
    (state, action) => (
      {
        ...adapter.upsertMany(action.drivers, state),
        timestamp: new Date().getTime()
      } as DriversState
    ))
);

export function reducer(state: DriversState | undefined, action: DriversActions.ActionsTypeGroup): DriversState { return driversReducer(state, action); }

export const selectDriversState = createFeatureSelector<DriversState>(driversFeatureKey);

export const { selectIds, selectEntities, selectAll, selectTotal } = adapter.getSelectors(selectDriversState);

export const selectDriverById = createSelector(
  selectEntities,
  (entities: Dictionary<Driver>, driverId: string) => entities[driverId]
);

export const selectTimestamp = createSelector(
  selectDriversState,
  (state: DriversState) => state.timestamp as number
);



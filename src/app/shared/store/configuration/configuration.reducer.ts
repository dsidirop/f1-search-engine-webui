import { EStandardStoreKeys } from '@app-shared/models/enums';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { createFeatureSelector, createReducer, createSelector, on } from '@ngrx/store';
import * as ConfigurationActions from './configuration.actions';
import { Configuration } from './configuration.model';

// currently the ngrx table below holds just one configuration at any given
// time the default one   the configuration is essentially the globalconfiguration
// loaded from the environment   however there are provisions already in place
// so that in the future the enable the user to effortesly switch configuration profiles

export const configurationsFeatureKey = 'configurations';

export interface ConfigurationsState extends EntityState<Configuration> {
  activeId: string;
}

export const adapter: EntityAdapter<Configuration> = createEntityAdapter<Configuration>({
  selectId: (configuration: Configuration) => configuration?.id,
  sortComparer: false
});

export const initialState: ConfigurationsState = adapter.getInitialState({
  activeId: EStandardStoreKeys.Default
} as ConfigurationsState);

export const configurationsReducer = createReducer(
  initialState,
  on(ConfigurationActions.UpsertConfiguration, (state, { configuration }) => {
    const newConfiguration = {
      ...configuration,
      id: configuration.id ?? EStandardStoreKeys.Default
    } as Configuration;
    return adapter.upsertOne(newConfiguration, state);
  })
);

export function reducer(state: ConfigurationsState | undefined, action: ConfigurationActions.ActionsTypeGroup): ConfigurationsState {
  return configurationsReducer(state, action);
}

export const selectConfigurationsState = createFeatureSelector<ConfigurationsState>(configurationsFeatureKey);

export const { selectIds, selectEntities, selectAll, selectTotal } = adapter.getSelectors(selectConfigurationsState);

export const selectActiveConfigurationId = createSelector(
  selectConfigurationsState,
  state => state.activeId
);

export const selectDefaultConfiguration = createSelector(
  selectEntities,
  selectActiveConfigurationId,
  (entities, activeId) => entities[activeId]
);

export const selectDriversViewportItemsCount = createSelector(
  selectDefaultConfiguration,
  (defaultEntity) => defaultEntity?.driversViewportItemsCount
);

export const selectRacingHistoryViewportItemsCount = createSelector(
  selectDefaultConfiguration,
  (defaultEntity) => defaultEntity?.racingHistoryViewportItemsCount
);

export const selectDefaultConfigurationDebounceFilterboxConfigDelay = createSelector(
  selectDefaultConfiguration,
  (defaultEntity) => defaultEntity?.searchboxDebounceTime
);

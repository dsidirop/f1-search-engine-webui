import { GlobalConfiguration } from '@app-shared/models';

export interface Configuration extends GlobalConfiguration {
  id: string; //set to "default" typically
}

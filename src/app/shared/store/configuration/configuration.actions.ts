import { createAction, props, union } from '@ngrx/store';
import { Configuration } from './configuration.model';

// medium.com/@gregor.woiwode/ngrx-8-meet-the-new-upcoming-factory-methods-of-the-next-major-release-a97a079cc089

export const UpsertConfiguration = createAction('[Configuration] Upsert Configuration', props<{ configuration: Partial<Configuration> }>());

const actions = union({ UpsertConfiguration });

export type ActionsTypeGroup = typeof actions;

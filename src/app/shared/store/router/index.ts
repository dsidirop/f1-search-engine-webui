// start:ng42.barrel
export * from './navigation-specs.model';
export * from './router.actions';
export * from './router.effects';
export * from './router.facade';
export * from './router.reducer';
// end:ng42.barrel

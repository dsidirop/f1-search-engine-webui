import { Location } from '@angular/common';
import { Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { EApp } from '@app-shared/models/enums';
import { RouterStateTitle } from '@app-shared/routing/custom-router-state-serializer';
import { UtilsService } from '@app-shared/services/utils.service';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { RouterNavigationAction, ROUTER_NAVIGATION } from '@ngrx/router-store';
import { select, Store } from '@ngrx/store';
import { from, of } from 'rxjs';
import { catchError, combineLatest, exhaustMap, map, switchMap } from 'rxjs/operators';
import * as fromCore from './../core-status/core-status.reducer';
import * as RouterActions from './router.actions';

// stackoverflow.com/a/50104369/863651

@Injectable()
export class RouterEffects {

  public navigateTo$ = createEffect(
    () => this.actions$.pipe(
      ofType(RouterActions.navigateTo),
      switchMap(({ path, query: queryParams, extras }) =>
        from(this.router.navigate(path, { queryParams, ...extras }))
          .pipe(
            // the success fail actions do get created but they dont register due
            // to dispatch=false   we keep them here for the sake of completeness
            map(() => of(RouterActions.navigateToSuccess({ path, query: queryParams, extras }))),
            catchError(err => of(RouterActions.navigateToFailure({ error: err })))
          )
      )
    ),
    { dispatch: false }
  );

  public navigateBack$ = createEffect(
    () => this.actions$.pipe(
      ofType(RouterActions.navigateBack),
      exhaustMap(
        () => this.utilsService
          .observableFromFunction$(() => this.location.back())
          .pipe(
            // the success fail actions do get created but they dont register due
            // to dispatch=false   we keep them here for the sake of completeness
            map(() => of(RouterActions.navigateBackSuccess())),
            catchError(err => of(RouterActions.navigateBackFailure({ error: err })))
          )
      )
    ),
    { dispatch: false }
  );

  public navigateForward$ = createEffect(
    () => this.actions$.pipe(
      ofType(RouterActions.navigateForward),
      exhaustMap(
        () => this.utilsService
          .observableFromFunction$(() => this.location.forward())
          .pipe(
            // the success fail actions do get created but they dont register due
            // to dispatch=false   we keep them here for the sake of completeness
            map(() => of(RouterActions.navigateForwardSuccess())),
            catchError(err => of(RouterActions.navigateForwardFailure({ error: err })))
          )
      )
    ),
    { dispatch: false }
  );

  public updateTitle$ = createEffect(
    () => this.actions$.pipe(
      ofType(ROUTER_NAVIGATION),
      combineLatest(
        this.coreStatusStore$.pipe(select(fromCore.selectTitle)),
        (action: RouterNavigationAction<RouterStateTitle>, title: string) => {
          const eventualTitle = [EApp.TitlePrefix, title]
            .map(x => x?.trim())
            .filter(x => x)
            .join(' - ');

          this.titleService.setTitle(eventualTitle);
        })
    ),
    { dispatch: false }
  );

  public constructor(
    private readonly actions$: Actions,

    private readonly router: Router,
    private readonly location: Location,
    private readonly titleService: Title,
    private readonly utilsService: UtilsService,
    private readonly coreStatusStore$: Store<fromCore.CoreStatusState>
  ) {
    // this.router.events.pipe(
    //   filter(event => event instanceof ActivationEnd)
    // ).subscribe((event: ActivationEnd) =>
    //   this.store.dispatch(new RouteChange({
    //     params: { ...event.snapshot.params },
    //     path: event.snapshot.routeConfig.path
    //   }))
    // );
  }
}

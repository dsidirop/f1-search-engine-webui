import { NavigationExtras } from '@angular/router';

export interface NavigationSpecs {
  path: (string | number)[];
  query?: unknown;
  extras?: NavigationExtras;
}

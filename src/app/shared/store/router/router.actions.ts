import { createAction, props } from '@ngrx/store';
import { NavigationSpecs } from './navigation-specs.model';

export const navigateTo = createAction('[Router] Navigate To [Request 🏃]', props<NavigationSpecs>());
export const navigateToSuccess = createAction('[Router] Navigate To [Success ✔️(🏃)]', props<NavigationSpecs>());
export const navigateToFailure = createAction('[Router] Navigate To [Failure ❌(🏃)]', props<{ error: string | Error }>());

export const navigateBack = createAction('[Router] Navigate Back [Request ↶]');
export const navigateBackSuccess = createAction('[Router] Navigate Back [Success ✔️(↶)]');
export const navigateBackFailure = createAction('[Router] Navigate Back [Failure ❌(↶)]', props<{ error: string | Error }>());

export const navigateForward = createAction('[Router] Navigate Forward [Request ↷]');
export const navigateForwardSuccess = createAction('[Router] Navigate Forward [Success ✔️(↷)]');
export const navigateForwardFailure = createAction('[Router] Navigate Forward [Failure ❌(↷)]', props<{ error: string | Error }>());

import { Injectable } from '@angular/core';
import * as fromRootStore from '@app-shared/store/root';
import { NavigationSpecs } from '@app-shared/store/router/navigation-specs.model';
import { navigateBack, navigateForward, navigateTo } from '@app-shared/store/router/router.actions';
import { Store } from '@ngrx/store';

@Injectable({ providedIn: 'root' })
export class RouterFacade {
  public constructor(private readonly rootStore$: Store<fromRootStore.RootState>) {
  }

  public navigateTo(payload: NavigationSpecs): void { this.rootStore$.dispatch(navigateTo(payload)); }
  public navigateBack(): void { this.rootStore$.dispatch(navigateBack()); }
  public navigateForward(): void { this.rootStore$.dispatch(navigateForward()); }
}

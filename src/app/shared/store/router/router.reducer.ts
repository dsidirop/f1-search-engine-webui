import { RouterStateTitle } from '@app-shared/routing/custom-router-state-serializer';
import * as fromRouter from '@ngrx/router-store';
import { createFeatureSelector, ActionReducerMap } from '@ngrx/store';

export const routerFeatureKey = 'router';

export interface RouterState {
  router: fromRouter.RouterReducerState<RouterStateTitle>;
}

export const reducersMap: ActionReducerMap<RouterState> = {
  router: fromRouter.routerReducer
};

export const getRouterState = createFeatureSelector<fromRouter.RouterReducerState<RouterStateTitle>>(routerFeatureKey);

import AppBaseException from './app-base.exception';

export class DriverNotFoundException extends AppBaseException {
    public readonly driverId: string;

    public constructor(driverId: string) {
        super(`[DS.GSDI01] [SAFEGUARD] Driver with id '${driverId}' not found!`);

        this.driverId = driverId;
    }
}



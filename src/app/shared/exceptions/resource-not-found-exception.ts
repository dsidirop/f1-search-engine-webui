import AppBaseException from './app-base.exception';

export class ResourceNotFoundException extends AppBaseException {
    public constructor(message: string) {
        super(message);
    }
}

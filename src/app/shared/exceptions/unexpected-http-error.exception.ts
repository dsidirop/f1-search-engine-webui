import AppBaseException from './app-base.exception';

export class UnexpectedHttpErrorException extends AppBaseException {
    public constructor(message: string) {
        super(message);
    }
}

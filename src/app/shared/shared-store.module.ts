/* eslint-disable @typescript-eslint/no-unused-vars */
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { LocalStorageService } from './services/local-storage.service';
import { DRIVERS_LOCAL_STORAGE_KEY, DRIVERS_METAREDUCER_TOKEN, DRIVERS_STORAGE_KEYS } from './store-persistence/drivers.tokens';
import { storageMetaReducer as driversStorageMetaReducer } from './store-persistence/storage-meta-reducer';
import * as fromConfiguration from './store/configuration/configuration.reducer';
import { CoreStatusEffects } from './store/core-status/core-status.effects';
import * as fromCoreStatus from './store/core-status/core-status.reducer';
import { DriversEffects } from './store/drivers/drivers.effects';
import * as fromDrivers from './store/drivers/drivers.reducer';
import { DriversState } from './store/drivers/drivers.reducer';

export function getDriversMetaReducerConfig(
  saveKeys: string[],
  localStorageKey: string,
  localStorageService: LocalStorageService
): { metaReducers: unknown[] } {
  return { metaReducers: [driversStorageMetaReducer(saveKeys, localStorageKey, localStorageService)] };
}

const nameof = <T>(key: keyof T, _instance?: T): keyof T => key;

@NgModule({
  imports: [
    StoreModule.forFeature(fromDrivers.driversFeatureKey, fromDrivers.reducer, DRIVERS_METAREDUCER_TOKEN),
    StoreModule.forFeature(fromCoreStatus.coreStatusFeatureKey, fromCoreStatus.reducer),
    StoreModule.forFeature(fromConfiguration.configurationsFeatureKey, fromConfiguration.reducer),
    EffectsModule.forFeature([
      DriversEffects,
      CoreStatusEffects,
      //ConfigurationEffects //none yet
    ]),
  ],
  exports: [StoreModule],
  providers: [
    {
      provide: DRIVERS_STORAGE_KEYS,
      useValue: [
        nameof<DriversState>('ids'),
        nameof<DriversState>('entities'),
        nameof<DriversState>('timestamp')
      ]
    },
    {
      provide: DRIVERS_LOCAL_STORAGE_KEY,
      useValue: 'app.state.drivers'
    },
    {
      provide: DRIVERS_METAREDUCER_TOKEN,
      deps: [DRIVERS_STORAGE_KEYS, DRIVERS_LOCAL_STORAGE_KEY, LocalStorageService],
      useFactory: getDriversMetaReducerConfig
    },
  ]
})

export class SharedStoreModule { }

import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { DriverProfileComponent } from './components/smarties';
import { ERouteParams } from './enums';

const driverProfileRouting: ModuleWithProviders = RouterModule.forChild([
  {
    path: `:${ERouteParams.DriverId}`, // path: `drivers/:${ERouteParams.DriverId}`,
    component: DriverProfileComponent
  }
]);

@NgModule({
  imports: [driverProfileRouting],
  exports: [RouterModule]
})

export class DriverProfileRoutingModule { }

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { DriverInfoComponent } from '@app-driver-profile/components/dummies/driver-info';
import { DriverRacingHistoryGridComponent, EGridXSortingMode, GridColumnFilteringRequest, GridColumnSortingTweakRequest } from '@app-driver-profile/components/dummies/driver-racing-history-grid';
import { ERouteParams } from '@app-driver-profile/enums';
import { DRHistoryGridColumnStatesMap } from '@app-driver-profile/store/drivers-profiles-ui-statuses/driver-profile-ui-status.model';
import * as DriverProfileUiStatusActions from '@app-driver-profile/store/drivers-profiles-ui-statuses/drivers-profiles-ui-statuses.actions';
import * as fromDriversProfilesUiStatusesStore from '@app-driver-profile/store/drivers-profiles-ui-statuses/drivers-profiles-ui-statuses.reducer';
import * as fromDriversRacingHistoriesStore from '@app-driver-profile/store/drivers-racing-histories/drivers-racing-histories.reducer';
import { AppSmartBaselineComponent } from '@app-shared/components/app-smart-baseline.component';
import { ColumnSpecs } from '@app-shared/components/dummies/grid/column-specs.model';
import { GridConfig } from '@app-shared/components/dummies/grid/grid-config.model';
import { GlobalConfigurationService } from '@app-shared/configuration/global-configuration.service';
import { DebounceInputConfig } from '@app-shared/directives';
import { DriverRacingHistoryEntry } from '@app-shared/models/driver-racing-history-entry.model';
import { EDriverRacingHistoryColumnIds, ESortMode } from '@app-shared/models/enums';
import { CarConstructorRaw } from '@app-shared/models/raw/driver-racing-history-data-raw/car-constructor-raw.model';
import { CircuitRaw } from '@app-shared/models/raw/driver-racing-history-data-raw/circuit-raw.model';
import { debounceTimeDynamically } from '@app-shared/operators/debounce-time-dynamically.operator';
import { UtilsService } from '@app-shared/services/utils.service';
import * as fromConfigurationStore from '@app-shared/store/configuration/configuration.reducer';
import * as fromCoreStatusStore from '@app-shared/store/core-status/core-status.reducer';
import { Driver } from '@app-shared/store/drivers/driver.model';
import * as fromDriversStore from '@app-shared/store/drivers/drivers.reducer';
import { RouterFacade } from '@app-shared/store/router/router.facade';
import { select, Store } from '@ngrx/store';
import { from as ixFrom } from 'ix/iterable';
import { filter as ixFilter, map as ixMap } from 'ix/iterable/operators';
import { get as _get } from 'lodash';
import { combineLatest, Observable, of } from 'rxjs';
import { filter, map, scan, switchMap, takeUntil, tap } from 'rxjs/operators';

// stackblitz.com/angular/rddkerpbbde

@Component({
  selector: 'app-driver-profile',
  templateUrl: './driver-profile.component.html',
  styleUrls: ['./driver-profile.component.scss'],
  providers: [DriverInfoComponent, DriverRacingHistoryGridComponent]
})

export class DriverProfileComponent extends AppSmartBaselineComponent implements OnInit {
  public driverBasicInfo$: Observable<Driver>;
  public driverRacingHistoryColumnStates$: Observable<DRHistoryGridColumnStatesMap>;
  public driverRacingHistoryEntriesProcessed$: Observable<DriverRacingHistoryEntry[]>;

  public debounceFilterboxConfig$: Observable<DebounceInputConfig>;
  public driverRacingHistoryViewportItemsCount$: Observable<number>;

  public constructor(
    //misc
    private readonly route: ActivatedRoute,
    //services
    private readonly utils: UtilsService,
    private readonly routerFacade: RouterFacade,
    private readonly globalConfiguration: GlobalConfigurationService,
    //stores
    private readonly driversStore$: Store<fromDriversStore.DriversState>,
    private readonly coreStatusStore$: Store<fromCoreStatusStore.CoreStatusState>,
    private readonly configurationsStore$: Store<fromConfigurationStore.ConfigurationsState>,
    private readonly driverRacingHistoryStore$: Store<fromDriversRacingHistoriesStore.DriverRacingHistoryState>,
    private readonly driverProfilesUiStatusesStore$: Store<fromDriversProfilesUiStatusesStore.DriversProfilesUiStatusesState>
  ) {
    super(coreStatusStore$);
  }

  public ngOnInit(): void { //0
    this
      .wireUpRouteMonitoring()
      .wireUpDriverBasicInfo()
      .wireUpDriverRacingHistoryColumnStates() //order
      .wireUpDriverRacingHistory() //order
      .wireUpDriversViewportItemsCount()
      .wireUpDebounceFilterboxConfigDelay()
      .dispatchSetCoreStatus({ enlargedCoreSectionMode: true, header: { url: null, text: null, showBackButton: true } });

    //0 stackoverflow.com/a/50707007/863651   we need to tread carefully considering the fact
    //  that ngoninit wont get called when we reroute to the same component   for instance
    //  /drivers/foo and /drivers/bar will both trigger this component
  }

  public backClicked = (): void => this.routerFacade.navigateBack();

  public columnFilteringRequest($event: GridColumnFilteringRequest): void {
    this.driverProfilesUiStatusesStore$.dispatch(
      DriverProfileUiStatusActions.racingHistoryFilterColumn({
        columnId: $event.columnId,
        filterTerm: $event.filterTerm
      })
    );
  }

  public columnSortingTweakRequest($event: GridColumnSortingTweakRequest): void { //sorting
    if ($event.mode === EGridXSortingMode.SingleColumn) { //left click
      this.driverProfilesUiStatusesStore$.dispatch(
        DriverProfileUiStatusActions.roundRobinRacingHistorySingleColumnSortingMode({
          columnId: $event.columnId
        })
      );
      return;
    }

    if ($event.mode === EGridXSortingMode.MultipleColumns) { //right click
      this.driverProfilesUiStatusesStore$.dispatch(
        DriverProfileUiStatusActions.roundRobinRacingHistoryMultiColumnSortingMode({
          columnId: $event.columnId
        })
      );
      return;
    }

    throw new Error(`[DPC.DRHCSTR01] [BUG] $event.mode=${$event.mode} unknown!`);
  }

  private wireUpDriverBasicInfo(): DriverProfileComponent {
    this.driverBasicInfo$ = this.driversStore$.pipe(
      select(fromDriversProfilesUiStatusesStore.selectActiveDriverBasicInfoX),
      tap(newActiveDriver => this.dispatchSetCoreStatus({ //sideeffect
        header: { url: newActiveDriver?.url, text: newActiveDriver?.name, showBackButton: true },
        tabTitle: `Profile - ${newActiveDriver?.name ?? 'Loading ...'}`
      })),
      takeUntil(this.ngUnsubscribe$)
    );

    return this;
  }

  private wireUpDriverRacingHistory(): DriverProfileComponent {
    const driverRacingHistoryEntries$ = this.driverRacingHistoryStore$.pipe(
      select(fromDriversProfilesUiStatusesStore.selectActiveDriverRacingHistoryEntriesX),
      takeUntil(this.ngUnsubscribe$)
    );

    this.driverRacingHistoryEntriesProcessed$ = combineLatest(
      driverRacingHistoryEntries$,
      this.driverRacingHistoryColumnStates$
    )
      .pipe(
        map(([driverRacingHistoryEntries, driverRacingHistoryColumnStates]) => ({ driverRacingHistoryEntries, driverRacingHistoryColumnStates })),
        filter(({ driverRacingHistoryEntries, driverRacingHistoryColumnStates }) => !!driverRacingHistoryEntries && !!driverRacingHistoryColumnStates),
        scan(
          (acc, current) => ({ ...current, count: acc.count + 1 }),
          {
            count: 0,
            driverRacingHistoryEntries: null as DriverRacingHistoryEntry[],
            driverRacingHistoryColumnStates: null as DRHistoryGridColumnStatesMap
          }
        ),
        debounceTimeDynamically(x => x.count === 1 ? 0 : this.globalConfiguration.gridColumnTweakDebounceTime), //mind initial loading
        map(({ driverRacingHistoryEntries, driverRacingHistoryColumnStates }) => ({
          filteredEntries: this.gridFilteringTransducer( //filtering
            driverRacingHistoryEntries,
            driverRacingHistoryColumnStates,
            this.DriverRacingHistoryGridConfig
          ),
          driverRacingHistoryColumnStates
        })),
        map(({ filteredEntries, driverRacingHistoryColumnStates }) => this.gridSorter(
          filteredEntries,
          driverRacingHistoryColumnStates
          //this.DriverRacingHistoryGridConfig
        )),
        takeUntil(this.ngUnsubscribe$)
      );

    return this;
  }

  private wireUpDriverRacingHistoryColumnStates(): DriverProfileComponent {
    this.driverRacingHistoryColumnStates$ = this.driverProfilesUiStatusesStore$.pipe(
      select(fromDriversProfilesUiStatusesStore.selectActiveRacingHistoryColumnStates),
      takeUntil(this.ngUnsubscribe$)
    );

    return this;
  }

  private wireUpRouteMonitoring(): DriverProfileComponent {
    this.route.paramMap.pipe(
      switchMap((params: ParamMap) => of(params.get(ERouteParams.DriverId)))
    ).subscribe(driverId => {
      this.driverProfilesUiStatusesStore$.dispatch(DriverProfileUiStatusActions.setProfileActiveDriverId({ driverId }));
    });

    return this;
  }

  private wireUpDriversViewportItemsCount(): DriverProfileComponent {
    this.driverRacingHistoryViewportItemsCount$ = this.configurationsStore$.pipe(
      select(fromConfigurationStore.selectRacingHistoryViewportItemsCount),
      filter(x => !!x), //vital
      takeUntil(this.ngUnsubscribe$)
    );

    return this;
  }

  private wireUpDebounceFilterboxConfigDelay(): DriverProfileComponent {
    this.debounceFilterboxConfig$ = this.configurationsStore$.pipe(
      select(fromConfigurationStore.selectDefaultConfigurationDebounceFilterboxConfigDelay),
      filter(x => !!x), //vital
      map(x => ({ delay: x } as DebounceInputConfig)),
      takeUntil(this.ngUnsubscribe$)
    );

    return this;
  }

  private gridFilteringTransducer = (
    _entries: DriverRacingHistoryEntry[],
    _columnStates: DRHistoryGridColumnStatesMap,
    _gridConfiguration: GridConfig<EDriverRacingHistoryColumnIds>
  ): DriverRacingHistoryEntry[] => Array.from(
    ixFrom(_entries) //transducer
      .pipe(
        ixMap( //order
          (x: DriverRacingHistoryEntry) => (
            {
              ...x, //purity
              dateHumanFriendly: this.utils.stringifyDateLocalFormat(x.dateTimestamp) //better keep this one here instead of in the store
            } as DriverRacingHistoryEntry
          )
        ),
        ixFilter( //order
          (rowData: DriverRacingHistoryEntry) => _gridConfiguration.columnsSpecs.every(columnSpecs => {
            const value = _get(rowData, columnSpecs.valuePath);
            const filterTerm = _columnStates.get(columnSpecs.columnId)?.filterTerm;
            const filterTermIsMatching = !filterTerm || columnSpecs.filteringFn(value, filterTerm);
            return filterTermIsMatching;
          })
        )
      )
  );

  private gridSorter = (
    _entries: DriverRacingHistoryEntry[],
    _columnStates: DRHistoryGridColumnStatesMap
  ) => {
    const sortedColumnStatesAndSpecs = Array
      .from(_columnStates.values())
      .filter(columnState => (columnState.sortMode ?? ESortMode.None) !== ESortMode.None)
      .sort((aColumnState, bColumnState) => aColumnState.sortChainIndex - bColumnState.sortChainIndex)
      .map(
        columnState => ({
          columnState,
          columnSpecs: this.DriverRacingHistoryGridConfig.columnsSpecs.find(x => x.columnId === columnState.id)
        })
      );
    if (sortedColumnStatesAndSpecs.length === 0) {
      return _entries;
    }

    return Array
      .from(_entries)
      .sort(
        (rowDataA: DriverRacingHistoryEntry, rowDataB: DriverRacingHistoryEntry) =>
          sortedColumnStatesAndSpecs
            .map(
              ({ columnState, columnSpecs }) => {
                const valuePath = columnSpecs.valuePathForSorting || columnSpecs.valuePath;
                const aValue = _get(rowDataA, valuePath);
                const bValue = _get(rowDataB, valuePath);
                const comparisonEvaluation = columnSpecs.sortingFn(aValue, bValue);
                return columnState.sortMode * comparisonEvaluation;
              }
            )
            .find(x => x !== 0)
      );
  }

  private readonly crPropName = (keyOfT: keyof CircuitRaw): string =>
    this.utils.nameof<CircuitRaw>(keyOfT);

  private readonly ccrPropName = (keyOfT: keyof CarConstructorRaw): string =>
    this.utils.nameof<CarConstructorRaw>(keyOfT);

  private readonly drhePropName = (keyOfT: keyof DriverRacingHistoryEntry): string =>
    this.utils.nameof<DriverRacingHistoryEntry>(keyOfT);

  private readonly DriverRacingHistoryGridConfig: GridConfig<EDriverRacingHistoryColumnIds> = {
    columnsSpecs: [
      {
        columnId: EDriverRacingHistoryColumnIds.Date,
        valuePath: this.drhePropName('dateHumanFriendly'),
        valuePathForSorting: this.drhePropName('dateTimestamp'),
        sortingFn: this.utils.compareDates,
        filteringFn: this.utils.stringContainsWithDeburrAndIgnoreCase
      } as ColumnSpecs<EDriverRacingHistoryColumnIds, string, Date>,
      {
        columnId: EDriverRacingHistoryColumnIds.Circuit,
        valuePath: `${this.drhePropName('circuitSpecs')}.${this.crPropName('circuitName')}`,
        sortingFn: this.utils.compareStringsDeburrAndIgnoreCase,
        filteringFn: this.utils.stringContainsWithDeburrAndIgnoreCase
      } as ColumnSpecs<EDriverRacingHistoryColumnIds, string, string>,
      {
        columnId: EDriverRacingHistoryColumnIds.Grid,
        valuePath: this.drhePropName('grid'),
        sortingFn: this.utils.compareAsNumbers,
        filteringFn: this.utils.numbersAreEqual
      } as ColumnSpecs<EDriverRacingHistoryColumnIds, number, number>,
      {
        columnId: EDriverRacingHistoryColumnIds.Laps,
        valuePath: this.drhePropName('laps'),
        sortingFn: this.utils.compareAsNumbers,
        filteringFn: this.utils.numbersAreEqual
      } as ColumnSpecs<EDriverRacingHistoryColumnIds, number, number>,
      {
        columnId: EDriverRacingHistoryColumnIds.Status,
        valuePath: this.drhePropName('status'),
        sortingFn: this.utils.compareStringsDeburrAndIgnoreCase,
        filteringFn: this.utils.stringContainsWithDeburrAndIgnoreCase
      } as ColumnSpecs<EDriverRacingHistoryColumnIds, string, string>,
      {
        columnId: EDriverRacingHistoryColumnIds.Number,
        valuePath: this.drhePropName('number'),
        sortingFn: this.utils.compareAsNumbers,
        filteringFn: this.utils.numbersAreEqual
      } as ColumnSpecs<EDriverRacingHistoryColumnIds, number, number>,
      {
        columnId: EDriverRacingHistoryColumnIds.Points,
        valuePath: this.drhePropName('points'),
        sortingFn: this.utils.compareAsNumbers,
        filteringFn: this.utils.numbersAreEqual
      } as ColumnSpecs<EDriverRacingHistoryColumnIds, number, number>,
      {
        columnId: EDriverRacingHistoryColumnIds.Position,
        valuePath: this.drhePropName('position'),
        sortingFn: this.utils.compareAsNumbers,
        filteringFn: this.utils.numbersAreEqual
      } as ColumnSpecs<EDriverRacingHistoryColumnIds, number, number>,
      // { columnId: EDriverRacingHistoryColumnIds.PositionText } //no need
      {
        columnId: EDriverRacingHistoryColumnIds.Team,
        valuePath: `${this.drhePropName('carConstructorSpecs')}.${this.ccrPropName('name')}`,
        sortingFn: this.utils.compareStringsDeburrAndIgnoreCase,
        filteringFn: this.utils.numbersAreEqual
      } as ColumnSpecs<EDriverRacingHistoryColumnIds, string, string>
    ]
  }
}

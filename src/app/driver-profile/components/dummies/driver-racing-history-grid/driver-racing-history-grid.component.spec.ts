import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DriverRacingHistoryGridComponent } from './driver-racing-history-grid.component';

describe('DriverRacingHistoryComponent', () => {
  let fixture: ComponentFixture<DriverRacingHistoryGridComponent>;
  let component: DriverRacingHistoryGridComponent;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DriverRacingHistoryGridComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DriverRacingHistoryGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

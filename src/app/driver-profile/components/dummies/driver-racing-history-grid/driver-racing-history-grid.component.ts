import { Component, EventEmitter, Input, OnChanges, Output, SimpleChange, SimpleChanges, ViewEncapsulation } from '@angular/core';
import { DRHistoryGridColumnStatesMap } from '@app-driver-profile/store/drivers-profiles-ui-statuses/driver-profile-ui-status.model';
import { DebouncedInputChangedEventArgs, DebounceInputConfig, DebounceInputDirective } from '@app-shared/directives/debounce-input.directive';
import { FocusOnLoadDirective } from '@app-shared/directives/focus-on-load.directive';
import { ColumnState } from '@app-shared/models/column-state.model';
import { DriverRacingHistoryEntry } from '@app-shared/models/driver-racing-history-entry.model';
import { EDriverRacingHistoryColumnIds, ESortMode } from '@app-shared/models/enums';
import { ScrollbarWidthService } from '@app-shared/services/scrollbar-width.service';
//import { Store } from '@ngrx/store';

@Component({
  selector: 'app-driver-racing-history-grid',
  templateUrl: './driver-racing-history-grid.component.html',
  styleUrls: ['./driver-racing-history-grid.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [FocusOnLoadDirective, DebounceInputDirective]
})
export class DriverRacingHistoryGridComponent implements OnChanges {

  @Input()
  public columnStates = new Map<EDriverRacingHistoryColumnIds, ColumnState<EDriverRacingHistoryColumnIds>>();

  @Input()
  public viewportItemsCount = 10; //todo  autorecalculate viewport height!

  @Input()
  public racingHistoryRecords = null as DriverRacingHistoryEntry[]; //loading state

  @Input()
  public debounceFilterboxConfig: DebounceInputConfig;

  @Output()
  public columnFilteringRequest = new EventEmitter<GridColumnFilteringRequest>();

  @Output()
  public columnSortingTweakRequest = new EventEmitter<GridColumnSortingTweakRequest>();

  public readonly EColumns = EDriverRacingHistoryColumnIds;
  public readonly ESortMode = ESortMode;
  public readonly ScrollbarWidth: number;

  public constructor(private readonly scrollbarWidthService: ScrollbarWidthService) {
    this.ScrollbarWidth = this.scrollbarWidthService.width;
  }

  public numberOfSortedColumns = 0;
  public ngOnChanges(changes: SimpleChanges): void {
    if (!changes?.columnStates)
      return;

    const {
      currentValue: newColumnStates
    }: {
      currentValue: DRHistoryGridColumnStatesMap
    } = changes.columnStates as SimpleChange;
    if (!newColumnStates)
      return;

    this.numberOfSortedColumns = Array
      .from(newColumnStates.values())
      .filter(x => x.sortMode !== ESortMode.None)
      .length;
  }

  public colFilterChanged($event: DebouncedInputChangedEventArgs, columnId: EDriverRacingHistoryColumnIds): void {
    this.columnFilteringRequest.emit({ columnId, filterTerm: $event.value });
  }

  public colHeaderLeftClicked(columnId: EDriverRacingHistoryColumnIds): void {
    this.columnSortingTweakRequest.emit({ columnId, mode: EGridXSortingMode.SingleColumn });
  }

  public colHeaderRightClicked(columnId: EDriverRacingHistoryColumnIds): boolean {
    this.columnSortingTweakRequest.emit({ columnId, mode: EGridXSortingMode.MultipleColumns });

    return false; //suppress the context menu of the browser
  }

  public racingHistoryRecordsTrackByFn = (index: number, entry: DriverRacingHistoryEntry): number =>
    entry?.dateTimestamp; //date=id
}

export interface GridColumnFilteringRequest {
  columnId: EDriverRacingHistoryColumnIds;
  filterTerm: string;
}

export interface GridColumnSortingTweakRequest {
  mode: EGridXSortingMode;
  columnId: EDriverRacingHistoryColumnIds;
}

export enum EGridXSortingMode {
  SingleColumn = 0,
  MultipleColumns = 1
}

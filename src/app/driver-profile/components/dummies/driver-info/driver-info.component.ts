import { Component, Input, ViewEncapsulation } from '@angular/core';
import { HumanAgePipe } from '@app-shared/pipes/human-age.pipe';
import { Driver } from '@app-shared/store/drivers';

@Component({
  selector: 'app-driver-info',
  templateUrl: './driver-info.component.html',
  styleUrls: ['./driver-info.component.scss'],
  providers: [HumanAgePipe],
  encapsulation: ViewEncapsulation.None
})
export class DriverInfoComponent {

  @Input()
  public driverBasicInfo = null as Driver;
}

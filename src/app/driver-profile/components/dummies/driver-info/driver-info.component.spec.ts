import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DriverInfoComponent } from './driver-info.component';

describe('DriverRacingHistoryComponent', () => {
  let fixture: ComponentFixture<DriverInfoComponent>;
  let component: DriverInfoComponent;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DriverInfoComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DriverInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { DriversProfilesUiStatusesEffects } from './store/drivers-profiles-ui-statuses/drivers-profiles-ui-statuses.effects';
import * as fromDriverProfile from './store/drivers-profiles-ui-statuses/drivers-profiles-ui-statuses.reducer';
import { DriversRacingHistoryEffects } from './store/drivers-racing-histories/drivers-racing-histories.effects';
import * as fromDriversRacingHistories from './store/drivers-racing-histories/drivers-racing-histories.reducer';

@NgModule({
  imports: [
    StoreModule.forFeature(fromDriverProfile.driversProfilesUiStatusesFeatureKey, fromDriverProfile.reducer),
    StoreModule.forFeature(fromDriversRacingHistories.driversRacingHistoriesFeatureKey, fromDriversRacingHistories.reducer),
    EffectsModule.forFeature([DriversProfilesUiStatusesEffects, DriversRacingHistoryEffects]),
  ],
  exports: [StoreModule]
})

export class DriverProfileStoreModule { }


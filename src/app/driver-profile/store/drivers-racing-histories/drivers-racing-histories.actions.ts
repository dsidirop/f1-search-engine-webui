import { Update } from '@ngrx/entity';
import { createAction, props, union } from '@ngrx/store';
import { DriverRacingHistory } from './drivers-racing-histories.model';

export const fetchDriverRacingHistoryRequest = createAction('[DriversRacingHistory/API] Fetch Driver Racing History [Request 💬]', props<{ id: string }>());
export const fetchDriverRacingHistoryRequestFailure = createAction('[DriversRacingHistory/API] Fetch Driver Racing History [Failure ❌]', props<{ error: string | Error }>());
export const fetchDriverRacingHistoryRequestSuccess /*=upsertDriversRacePerformance*/ = createAction('[DriversRacingHistory/API] Fetch Driver Racing History [Success ✔️]', props<{ driverRacingHistory: DriverRacingHistory }>());

export const cacheCheckForDriverRacingHistory = createAction('[DriversRacingHistory] Cache-Check for Driver Racing History [Request 💬]', props<{ id: string }>());
export const cacheCheckForDriverRacingHistoryHit = createAction('[DriversRacingHistory] Cache-Check for Driver Racing History [Cache-Hit ✔️(🖴)]');
export const cacheCheckForDriverRacingHistoryMiss = createAction('[DriversRacingHistory] Cache-Check for Driver Racing History [Cache-Miss ⚡(🖴)]', props<{ id: string }>());

export const addDriverRacingHistory = createAction('[DriversRacingHistory] Add Driver Racing History', props<{ driverRacingHistory: DriverRacingHistory }>());
export const deleteDriverRacingHistory = createAction('[DriversRacingHistory] Delete Driver Racing History', props<{ id: string }>());
export const upsertDriverRacingHistory = createAction('[DriversRacingHistory] Upsert Driver Racing History', props<{ driverRacingHistory: DriverRacingHistory }>());
export const updateDriverRacingHistory = createAction('[DriversRacingHistory] Update Driver Racing History', props<{ driverRacingHistory: Update<DriverRacingHistory> }>());

export const addDriversRacingHistories = createAction('[DriversRacingHistory] Add Drivers Racing Histories', props<{ driversRacingHistories: DriverRacingHistory[] }>());
export const loadDriversRacingHistories = createAction('[DriversRacingHistory] Load Drivers Racing Histories', props<{ driversRacingHistories: DriverRacingHistory[] }>());
export const clearDriversRacingHistories = createAction('[DriversRacingHistory] Clear Drivers Racing Histories');
export const deleteDriversRacingHistories = createAction('[DriversRacingHistory] Delete Drivers Racing Histories', props<{ ids: string[] }>());
export const upsertDriversRacingHistories = createAction('[DriversRacingHistory] Upsert Drivers Racing Histories', props<{ driversRacingHistories: DriverRacingHistory[] }>());
export const updateDriversRacingHistories = createAction('[DriversRacingHistory] Update Drivers Racing Histories', props<{ driversRacingHistories: Update<DriverRacingHistory>[] }>());

const actions = union({
  fetchDriverRacingHistoryRequest,
  fetchDriverRacingHistoryRequestFailure,
  fetchDriverRacingHistoryRequestSuccess,

  cacheCheckForDriverRacingHistory,
  cacheCheckForDriverRacingHistoryHit,
  cacheCheckForDriverRacingHistoryMiss,

  addDriverRacingHistory,
  updateDriverRacingHistory,
  deleteDriverRacingHistory,
  upsertDriverRacingHistory,

  addDriversRacingHistories,
  loadDriversRacingHistories,
  clearDriversRacingHistories,
  deleteDriversRacingHistories,
  upsertDriversRacingHistories,
  updateDriversRacingHistories
});

export type ActionsTypeGroup = typeof actions;

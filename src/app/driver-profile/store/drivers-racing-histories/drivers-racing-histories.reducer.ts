import { createEntityAdapter, Dictionary, EntityAdapter, EntityState } from '@ngrx/entity';
import { createFeatureSelector, createReducer, createSelector, on } from '@ngrx/store';
import * as DriverRacingHistoryActions from './drivers-racing-histories.actions';
import { DriverRacingHistory } from './drivers-racing-histories.model';

export const driversRacingHistoriesFeatureKey = 'driversRacingHistories'; //each driver has his own race-log

export type DriverRacingHistoryState = EntityState<DriverRacingHistory>

export const adapter: EntityAdapter<DriverRacingHistory> = createEntityAdapter<DriverRacingHistory>({
  selectId: (profileStatus: DriverRacingHistory) => profileStatus?.id,
  sortComparer: false
});

export const initialState: DriverRacingHistoryState = adapter.getInitialState({
  ids: [],
  entities: {}
} as DriverRacingHistoryState);

const driverProfileReducer = createReducer(
  initialState,
  //single
  on(DriverRacingHistoryActions.addDriverRacingHistory, (state, action) => adapter.addOne(action.driverRacingHistory, state)),
  on(DriverRacingHistoryActions.deleteDriverRacingHistory, (state, action) => adapter.removeOne(action.id, state)),
  on(DriverRacingHistoryActions.updateDriverRacingHistory, (state, action) => adapter.updateOne(action.driverRacingHistory, state)),
  on(
    DriverRacingHistoryActions.upsertDriverRacingHistory,
    DriverRacingHistoryActions.fetchDriverRacingHistoryRequestSuccess,
    (state, action) => adapter.upsertOne(action.driverRacingHistory, state)
  ),
  //many
  on(DriverRacingHistoryActions.addDriversRacingHistories, (state, action) => adapter.addMany(action.driversRacingHistories, state)),
  on(DriverRacingHistoryActions.loadDriversRacingHistories, (state, action) => adapter.addAll(action.driversRacingHistories, state)),
  on(DriverRacingHistoryActions.clearDriversRacingHistories, (state) => adapter.removeAll(state)),
  on(DriverRacingHistoryActions.deleteDriversRacingHistories, (state, action) => adapter.removeMany(action.ids, state)),
  on(DriverRacingHistoryActions.upsertDriversRacingHistories, (state, action) => adapter.upsertMany(action.driversRacingHistories, state)),
  on(DriverRacingHistoryActions.updateDriversRacingHistories, (state, action) => adapter.updateMany(action.driversRacingHistories, state))
);

export function reducer(state: DriverRacingHistoryState | undefined, action: DriverRacingHistoryActions.ActionsTypeGroup): DriverRacingHistoryState {
  return driverProfileReducer(state, action);
}

export const selectDriversRacingHistoriesState = createFeatureSelector<DriverRacingHistoryState>(driversRacingHistoriesFeatureKey);

export const { selectIds, selectEntities, selectAll, selectTotal } = adapter.getSelectors(selectDriversRacingHistoriesState);

export const selectDriverRacingHistoryById = createSelector(
  selectEntities,
  (entities: Dictionary<DriverRacingHistory>, driverId: string) => entities[driverId] as DriverRacingHistory
);

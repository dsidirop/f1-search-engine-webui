import { DriverRacingHistoryEntry } from '@app-shared/models';

export interface DriverRacingHistory {
  id: string; //driverid

  entries: DriverRacingHistoryEntry[]; //grid
  timestamp: number;
}

import { Injectable } from '@angular/core';
import { GlobalConfigurationService } from '@app-shared/configuration';
import { ResourceNotFoundException } from '@app-shared/exceptions';
import { DriversService } from '@app-shared/services';
import * as coreStatusActions from '@app-shared/store/core-status/core-status.actions';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { of } from 'rxjs';
import { catchError, concatMap, map, mergeMap, withLatestFrom } from 'rxjs/operators';
import * as driversRacingHistoryActions from './drivers-racing-histories.actions';
import { DriverRacingHistory } from './drivers-racing-histories.model';
import * as fromDriverRacingHistoryStore from './drivers-racing-histories.reducer';

@Injectable()
export class DriversRacingHistoryEffects {

  public cacheCheckForDriverRacingHistory$ = createEffect(
    () => this.actions$.pipe(
      ofType(driversRacingHistoryActions.cacheCheckForDriverRacingHistory),
      concatMap(action => of(action).pipe(
        withLatestFrom(this.driverRacingHistoryStore$.pipe(
          select(fromDriverRacingHistoryStore.selectDriverRacingHistoryById, action.id),
          map(cachedDriverRacingHistory => this.isCacheHit(cachedDriverRacingHistory))
        ))
      )),
      map(
        ([action, isCacheHit]) => isCacheHit
          ? driversRacingHistoryActions.cacheCheckForDriverRacingHistoryHit()
          : driversRacingHistoryActions.cacheCheckForDriverRacingHistoryMiss({ id: action.id }) // => fetchDriverRacingHistoryRequest
      )
    )
  );

  public fetchDriverRacingHistory$ = createEffect(
    () => this.actions$.pipe(
      ofType(
        driversRacingHistoryActions.fetchDriverRacingHistoryRequest,
        driversRacingHistoryActions.cacheCheckForDriverRacingHistoryMiss
      ),
      mergeMap(
        action => this.driversService
          .getRacingHistory(action.id)
          .pipe(
            map(
              driverRacingHistoryEntries => driversRacingHistoryActions.fetchDriverRacingHistoryRequestSuccess({
                driverRacingHistory: {
                  id: action.id,
                  entries: driverRacingHistoryEntries,
                  timestamp: new Date().getTime()
                }
              })
            ),
            catchError(error => of(driversRacingHistoryActions.fetchDriverRacingHistoryRequestFailure({ error })))
          )
      )
    )
  );

  public fetchDriverRacingHistoryRequestFailure$ = createEffect(
    () => this.actions$.pipe(
      ofType(driversRacingHistoryActions.fetchDriverRacingHistoryRequestFailure),
      map(
        action => action.error as ResourceNotFoundException
          ? coreStatusActions.upsertServiceOfflineErrorCoreStatus()
          : coreStatusActions.upsertUnexpectedErrorCoreStatus({ error: action.error })
      )
    )
  );

  private isCacheHit(entry: DriverRacingHistory): boolean {
    const threshold = this.globalConfigurationService.fetchDriverRacingHistoryThrottlingTime;
    return entry && (new Date().getTime() - entry.timestamp) <= threshold;
  }

  public constructor(
    //misc
    private readonly actions$: Actions,
    //services
    private readonly driversService: DriversService,
    private readonly globalConfigurationService: GlobalConfigurationService,
    //stores
    private readonly driverRacingHistoryStore$: Store<fromDriverRacingHistoryStore.DriverRacingHistoryState>
  ) {
  }
}

import { Injectable } from '@angular/core';
import * as driversRacingHistoriesActions from '@app-driver-profile/store/drivers-racing-histories/drivers-racing-histories.actions';
import { ColumnState } from '@app-shared/models/column-state.model';
import { EDriverRacingHistoryColumnIds, ESortMode } from '@app-shared/models/enums';
import { notNil } from '@app-shared/operators';
import { UtilsService } from '@app-shared/services/utils.service';
import * as driversActions from '@app-shared/store/drivers/drivers.actions.ts';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { isNil as _isNil } from 'lodash';
import { of } from 'rxjs';
import { catchError, concatMap, map, withLatestFrom } from 'rxjs/operators';
import { DRHistoryGridColumnStatesMap } from './driver-profile-ui-status.model';
import * as driversProfilesUiStatusesActions from './drivers-profiles-ui-statuses.actions';
import * as fromDriversProfilesUiStatusesStore from './drivers-profiles-ui-statuses.reducer';

@Injectable()
export class DriversProfilesUiStatusesEffects {

  public setProfileActiveDriverId$ = createEffect(
    () => this.actions$.pipe(
      ofType(driversProfilesUiStatusesActions.setProfileActiveDriverId),
      map(action => action.driverId),
      notNil(),
      concatMap(driverId => [ //0 mild antipattern indeed
        driversActions.cacheCheckForDriver({ id: driverId }),
        driversRacingHistoriesActions.cacheCheckForDriverRacingHistory({ id: driverId }),
        driversProfilesUiStatusesActions.cacheCheckForDriverProfileUiStatus({ id: driverId })
      ]),
      catchError(error => of(driversProfilesUiStatusesActions.setProfileActiveDriverIdFailure({ error })))
    )

    //0     medium.com/@amcdnl/dispatching-multiple-actions-from-ngrx-effects-c1447ceb6b22
    //
    //  dispatching more than one actions is a known mild antipattern   the alternative is to use
    //  the following package but its a bit of an overkill
    //
    //                        github.com/tshelburne/redux-batched-actions
    //
  );

  public racingHistoryFilterColumn$ = createEffect(
    () => this.actions$.pipe(
      ofType(driversProfilesUiStatusesActions.racingHistoryFilterColumn),
      concatMap(
        action => of(action).pipe(
          withLatestFrom(
            this.driversProfilesUiStatusesStore$.pipe(select(fromDriversProfilesUiStatusesStore.selectActiveDriverId)),
            this.driversProfilesUiStatusesStore$.pipe(select(fromDriversProfilesUiStatusesStore.selectActiveRacingHistoryColumnStates))
          )
        )
      ),
      map(
        ([action, activeDriverId, currentColumnStates]) => {
          const newColumnStates = this.deepCloneColumnStates(currentColumnStates);
          let currentColumnState = newColumnStates.get(action.columnId);
          if (!currentColumnState) { //column state doesnt preexist
            currentColumnState = {
              id: action.columnId,
              sortMode: ESortMode.None,
              filterTerm: '',
              sortChainIndex: null
            } as ColumnState<EDriverRacingHistoryColumnIds>;

            newColumnStates.set(action.columnId, currentColumnState);
          }

          currentColumnState.filterTerm = action.filterTerm;

          return activeDriverId
            ? driversProfilesUiStatusesActions.racingHistoryFilterColumnSuccess({
              driverId: activeDriverId,
              newColumnStates: newColumnStates
            })
            : driversProfilesUiStatusesActions.racingHistoryFilterColumnFailure({ error: 'No Driver Profile is Active!' });
        }
      )
    )
  );

  public cacheCheckForDriverProfileUiStatus$ = createEffect(
    () => this.actions$.pipe(
      ofType(driversProfilesUiStatusesActions.cacheCheckForDriverProfileUiStatus),
      concatMap(action => of(action).pipe(
        withLatestFrom(this.driversProfilesUiStatusesStore$.pipe(
          select(fromDriversProfilesUiStatusesStore.selectUiStatusById, action.id),
          map(cachedDriverProfileUiStatus => !!cachedDriverProfileUiStatus)
        ))
      )),
      map(
        ([action, isCacheHit]) => isCacheHit
          ? driversProfilesUiStatusesActions.cacheCheckForDriverProfileUiStatusHit()
          : driversProfilesUiStatusesActions.cacheCheckForDriverProfileUiStatusMiss({ id: action.id }) // => fetchDriverProfileRequest
      )
    )
  );

  public cacheCheckForDriverProfileUiStatusMiss$ = createEffect(
    () => this.actions$.pipe(
      ofType(driversProfilesUiStatusesActions.cacheCheckForDriverProfileUiStatusMiss),
      concatMap(action => [
        driversProfilesUiStatusesActions.upsertDriverProfileUiStatus({
          driverProfileUiStatus: {
            id: action.id,
            racingHistoryGrid: {
              columnStates: DriversProfilesUiStatusesEffects.EmptyColumnStates
            }
          }
        }),
        driversProfilesUiStatusesActions.roundRobinRacingHistorySingleColumnSortingMode({
          columnId: EDriverRacingHistoryColumnIds.Date //0
        })
      ])
    )

    //0 we want the grid to be sorted by date in descending mode by default
  );

  public roundRobinRacingHistorySingleColumnSortingMode$ = createEffect(
    () => this.actions$.pipe(
      ofType(driversProfilesUiStatusesActions.roundRobinRacingHistorySingleColumnSortingMode),
      concatMap(
        action => of(action).pipe(
          withLatestFrom(
            this.driversProfilesUiStatusesStore$.pipe(select(fromDriversProfilesUiStatusesStore.selectActiveDriverId)),
            this.driversProfilesUiStatusesStore$.pipe(select(fromDriversProfilesUiStatusesStore.selectActiveRacingHistoryColumnStates))
          )
        )
      ),
      map(
        ([action, activeDriverId, currentColumnStates]) => {
          const newColumnStates = this.deepCloneColumnStates(currentColumnStates);
          let currentColumnState = newColumnStates.get(action.columnId);
          if (!currentColumnState) { //column state doesnt preexist
            currentColumnState = {
              id: action.columnId,
              sortMode: ESortMode.None,
              filterTerm: '',
              sortChainIndex: null
            } as ColumnState<EDriverRacingHistoryColumnIds>;

            newColumnStates.set(action.columnId, currentColumnState);
          }

          currentColumnState.sortMode = this.utils.getNextSortMode(currentColumnState.sortMode);
          currentColumnState.sortChainIndex = currentColumnState.sortMode === ESortMode.None ? null : 1;

          Array
            .from(newColumnStates.values())
            .filter(x => x.id !== action.columnId)
            .forEach(x => {
              x.sortMode = ESortMode.None;
              x.sortChainIndex = null;
            });

          return activeDriverId
            ? driversProfilesUiStatusesActions.roundRobinRacingHistorySingleColumnSortingModeSuccess({
              driverId: activeDriverId,
              newColumnStates: newColumnStates
            })
            : driversProfilesUiStatusesActions.roundRobinRacingHistorySingleColumnSortingModeFailure({ error: 'No Driver Profile is Active!' });
        }
      )
    )
  );

  public roundRobinRacingHistoryMultiColumnSortingMode$ = createEffect(
    () => this.actions$.pipe(
      ofType(driversProfilesUiStatusesActions.roundRobinRacingHistoryMultiColumnSortingMode),
      concatMap(
        action => of(action).pipe(
          withLatestFrom(
            this.driversProfilesUiStatusesStore$.pipe(select(fromDriversProfilesUiStatusesStore.selectActiveDriverId)),
            this.driversProfilesUiStatusesStore$.pipe(select(fromDriversProfilesUiStatusesStore.selectActiveRacingHistoryColumnStates))
          )
        )
      ),
      map(
        ([action, activeDriverId, currentColumnStates]) => {
          const newColumnStates = this.deepCloneColumnStates(currentColumnStates);
          let currentColumnState = newColumnStates.get(action.columnId);
          if (!currentColumnState) { //column state doesnt preexist
            currentColumnState = {
              id: action.columnId,
              sortMode: ESortMode.None,
              filterTerm: '',
              sortChainIndex: null
            } as ColumnState<EDriverRacingHistoryColumnIds>;

            newColumnStates.set(action.columnId, currentColumnState);
          }

          currentColumnState.sortMode = this.utils.getNextSortMode(currentColumnState.sortMode); //new sort mode
          if (currentColumnState.sortMode === ESortMode.None) {
            currentColumnState.sortChainIndex = null;
            Array
              .from(newColumnStates.values())
              .filter(x => x.sortMode !== ESortMode.None)
              .sort(x => x.sortChainIndex)
              .forEach((x, i) => { //            we squeeze out any gap that might have been
                x.sortChainIndex = i + 1; //     created in the sortchainindeces sequence
              });
          } else if (_isNil(currentColumnState.sortChainIndex)) {
            currentColumnState.sortChainIndex = 1 + Math.max.apply(
              null,
              [{ sortChainIndex: 0 }]
                .concat(Array.from(newColumnStates.values()))
                .map(x => x.sortChainIndex)
                .filter(x => !_isNil(x))
            );
          }

          return activeDriverId
            ? driversProfilesUiStatusesActions.roundRobinRacingHistoryMultiColumnSortingModeSuccess({
              driverId: activeDriverId,
              newColumnStates: newColumnStates
            })
            : driversProfilesUiStatusesActions.roundRobinRacingHistoryMultiColumnSortingModeFailure({
              error: 'No Driver Profile is Active!'
            });
        }
      )
    )
  );

  private deepCloneColumnStates = (columnStates: DRHistoryGridColumnStatesMap) => //shorthand
    this.utils.deepClone<DRHistoryGridColumnStatesMap>(
      columnStates ?? DriversProfilesUiStatusesEffects.EmptyColumnStates
    );

  private static readonly EmptyColumnStates = new Map();

  public constructor(
    //misc
    private readonly actions$: Actions,
    //services
    private readonly utils: UtilsService,
    //stores
    private readonly driversProfilesUiStatusesStore$: Store<fromDriversProfilesUiStatusesStore.DriversProfilesUiStatusesState>
  ) {
  }
}


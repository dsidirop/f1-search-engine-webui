/* eslint-disable @typescript-eslint/no-explicit-any */
import { initialState, reducer } from './drivers-profiles-ui-statuses.reducer';

describe('DriverProfile Status Reducer', () => {
  describe('unknown action', () => {
    it('should return the previous state', () => {
      const action = {} as any;

      const result = reducer(initialState, action);

      expect(result).toBe(initialState);
    });
  });
});

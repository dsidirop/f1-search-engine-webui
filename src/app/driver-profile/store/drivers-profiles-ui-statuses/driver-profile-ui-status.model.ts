import { ColumnState } from '@app-shared/models/column-state.model';
import { EDriverRacingHistoryColumnIds } from '@app-shared/models/enums';

export type DRHistoryGridColumnStatesMap = Map<EDriverRacingHistoryColumnIds, ColumnState<EDriverRacingHistoryColumnIds>>

export interface DriverProfileUiStatus {
  id: string; //driverid

  racingHistoryGrid: {
    columnStates: DRHistoryGridColumnStatesMap; //grid  column-name -> column-state
  }
}

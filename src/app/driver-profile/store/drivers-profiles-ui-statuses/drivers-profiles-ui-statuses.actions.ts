import { EDriverRacingHistoryColumnIds } from '@app-shared/models/enums';
import { Update } from '@ngrx/entity';
import { createAction, props, union } from '@ngrx/store';
import { DRHistoryGridColumnStatesMap, DriverProfileUiStatus } from './driver-profile-ui-status.model';

export const cacheCheckForDriverProfileUiStatus = createAction('[DriverProfileUIStatus] Cache-Check for Driver Profile UΙ Status [Request 💬]', props<{ id: string }>());
export const cacheCheckForDriverProfileUiStatusHit = createAction('[DriverProfileUIStatus] Cache-Check for Driver Profile UΙ Status [Cache-Hit ✔️(🖴)]');
export const cacheCheckForDriverProfileUiStatusMiss = createAction('[DriverProfileUIStatus] Cache-Check for Driver Profile UΙ Status [Cache-Miss ⚡(🖴)]', props<{ id: string }>());

export const setProfileActiveDriverId = createAction('[DriverProfileUIStatus/API] Set Profile Active Driver Id [Request 💬]', props<{ driverId: string }>());
export const setProfileActiveDriverIdFailure = createAction('[DriverProfileUIStatus/API] Set Profile Active Driver Id [Failure ❌]', props<{ error: string | Error }>());

export const racingHistoryFilterColumn = createAction('[DriverProfileUIStatus] Driver Racing History Filter Column [Request 💬]', props<{ columnId: EDriverRacingHistoryColumnIds, filterTerm: string }>());
export const racingHistoryFilterColumnFailure = createAction('[DriverProfileUIStatus] Driver Racing History Filter Column [Failure ❌]', props<{ error: string }>());
export const racingHistoryFilterColumnSuccess = createAction('[DriverProfileUIStatus] Driver Racing History Filter Column [Success ✔️]', props<{
  driverId: string,
  newColumnStates: DRHistoryGridColumnStatesMap
}>());

export const roundRobinRacingHistorySingleColumnSortingMode = createAction('[DriverProfileUIStatus] Driver Racing History Round Robin Single-Column Sorting Mode [Request 💬]', props<{ columnId: EDriverRacingHistoryColumnIds }>());
export const roundRobinRacingHistorySingleColumnSortingModeFailure = createAction('[DriverProfileUIStatus] Driver Racing History Round Robin Single-Column Sorting Mode [Failure ❌]', props<{ error: string }>());
export const roundRobinRacingHistorySingleColumnSortingModeSuccess = createAction('[DriverProfileUIStatus] Driver Racing History Round Robin Single-Column Sorting Mode [Success ✔️]', props<{
  driverId: string,
  newColumnStates: DRHistoryGridColumnStatesMap
}>());

export const roundRobinRacingHistoryMultiColumnSortingMode = createAction('[DriverProfileUIStatus] Round Robin Multi-Column Sorting Mode [Request 💬]', props<{ columnId: EDriverRacingHistoryColumnIds }>());
export const roundRobinRacingHistoryMultiColumnSortingModeFailure = createAction('[DriverProfileUIStatus] Round Robin Multi-Column Sorting Mode [Success ❌]', props<{ error: string }>());
export const roundRobinRacingHistoryMultiColumnSortingModeSuccess = createAction('[DriverProfileUIStatus] Round Robin Multi-Column Sorting Mode [Success ✔️]', props<{
  driverId: string,
  newColumnStates: DRHistoryGridColumnStatesMap
}>());

//singles
export const addDriverProfileUiStatus = createAction('[DriverProfileUIStatus] Add Driver Profile UΙ Status', props<{ driverProfileUiStatus: DriverProfileUiStatus }>());
export const deleteDriverProfileUiStatus = createAction('[DriverProfileUIStatus] Delete Driver Profile UΙ Status', props<{ id: string }>());
export const upsertDriverProfileUiStatus = createAction('[DriverProfileUIStatus] Upsert Driver Profile UΙ Status', props<{ driverProfileUiStatus: DriverProfileUiStatus }>());
export const updateDriverProfileUiStatus = createAction('[DriverProfileUIStatus] Update Driver Profile UΙ Status', props<{ driverProfileUiStatus: Update<DriverProfileUiStatus> }>());
//multiples
export const addDriversProfilesUiStatuses = createAction('[DriverProfileUIStatus] Add Drivers Profiles UΙ Statuses', props<{ driversProfilesUiStatuses: DriverProfileUiStatus[] }>());
export const loadDriversProfilesUiStatuses = createAction('[DriverProfileUIStatus] Load Drivers Profiles UΙ Statuses', props<{ driversProfilesUiStatuses: DriverProfileUiStatus[] }>());
export const clearDriversProfilesUiStatuses = createAction('[DriverProfileUIStatus] Clear Drivers Profiles UΙ Statuses');
export const deleteDriversProfilesUiStatuses = createAction('[DriverProfileUIStatus] Delete Drivers Profiles UΙ Statuses', props<{ ids: string[] }>());
export const upsertDriversProfilesUiStatuses = createAction('[DriverProfileUIStatus] Upsert Drivers Profiles UΙ Statuses', props<{ driversProfilesUiStatuses: DriverProfileUiStatus[] }>());
export const updateDriversProfilesUiStatuses = createAction('[DriverProfileUIStatus] Update Drivers Profiles UΙ Statuses', props<{ driversProfilesUiStatuses: Update<DriverProfileUiStatus>[] }>());

const actions = union({
  cacheCheckForDriverProfileUiStatus,
  cacheCheckForDriverProfileUiStatusHit,
  cacheCheckForDriverProfileUiStatusMiss,

  setProfileActiveDriverId,
  setProfileActiveDriverIdFailure,

  roundRobinRacingHistoryMultiColumnSortingMode,
  roundRobinRacingHistoryMultiColumnSortingModeFailure,
  roundRobinRacingHistoryMultiColumnSortingModeSuccess,

  addDriverProfileUiStatus,
  deleteDriverProfileUiStatus,
  upsertDriverProfileUiStatus,
  updateDriverProfileUiStatus,

  addDriversProfilesUiStatuses,
  loadDriversProfilesUiStatuses,
  clearDriversProfilesUiStatuses,
  deleteDriversProfilesUiStatuses,
  upsertDriversProfilesUiStatuses,
  updateDriversProfilesUiStatuses
});

export type ActionsTypeGroup = typeof actions;

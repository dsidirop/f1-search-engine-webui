import { EDriverRacingHistoryColumnIds } from '@app-shared';
import { ColumnState } from '@app-shared/models/column-state.model';
import { Driver } from '@app-shared/store/drivers/driver.model';
import * as fromDriversStore from '@app-shared/store/drivers/drivers.reducer';
import { createEntityAdapter, Dictionary, EntityAdapter, EntityState } from '@ngrx/entity';
import { createFeatureSelector, createReducer, createSelector, on } from '@ngrx/store';
import { DriverRacingHistory } from '../drivers-racing-histories';
import * as fromDriversRacingHistoriesStore from '../drivers-racing-histories/drivers-racing-histories.reducer';
import { DriverProfileUiStatus } from './driver-profile-ui-status.model';
import * as DriversProfilesUiStatusesActions from './drivers-profiles-ui-statuses.actions';

export const driversProfilesUiStatusesFeatureKey = 'driversProfilesUiStatuses'; //each driver has his own profile-ui-status

export interface DriversProfilesUiStatusesState extends EntityState<DriverProfileUiStatus> {
  activeDriverId: string; //active in /drivers/:driverid
}

export const adapter: EntityAdapter<DriverProfileUiStatus> = createEntityAdapter<DriverProfileUiStatus>({
  selectId: (driverProfileUiStatus: DriverProfileUiStatus) => driverProfileUiStatus?.id,
  sortComparer: false
});

export const initialState: DriversProfilesUiStatusesState = adapter.getInitialState({
  ids: [],
  entities: {},
  activeDriverId: null
} as DriversProfilesUiStatusesState);

const driverProfileStatusReducer = createReducer(
  initialState,
  on(DriversProfilesUiStatusesActions.setProfileActiveDriverId, (state, action) => ({ ...state, activeDriverId: action.driverId })),
  //singles
  on(DriversProfilesUiStatusesActions.addDriverProfileUiStatus, (state, action) => adapter.addOne(action.driverProfileUiStatus, state)),
  on(DriversProfilesUiStatusesActions.deleteDriverProfileUiStatus, (state, action) => adapter.removeOne(action.id, state)),
  on(DriversProfilesUiStatusesActions.upsertDriverProfileUiStatus, (state, action) => adapter.upsertOne(action.driverProfileUiStatus, state)),
  on(DriversProfilesUiStatusesActions.updateDriverProfileUiStatus, (state, action) => adapter.updateOne(action.driverProfileUiStatus, state)),
  on(
    DriversProfilesUiStatusesActions.racingHistoryFilterColumnSuccess,
    DriversProfilesUiStatusesActions.roundRobinRacingHistoryMultiColumnSortingModeSuccess,
    DriversProfilesUiStatusesActions.roundRobinRacingHistorySingleColumnSortingModeSuccess,
    (state, action) => {
      const newDriverProfileUiStatus = {
        id: action.driverId,
        racingHistoryGrid: {
          columnStates: action.newColumnStates
        }
      } as DriverProfileUiStatus;

      return adapter.upsertOne(newDriverProfileUiStatus, state);
    }
  ),
  //multiples
  on(DriversProfilesUiStatusesActions.addDriversProfilesUiStatuses, (state, action) => adapter.addMany(action.driversProfilesUiStatuses, state)),
  on(DriversProfilesUiStatusesActions.loadDriversProfilesUiStatuses, (state, action) => adapter.addAll(action.driversProfilesUiStatuses, state)),
  on(DriversProfilesUiStatusesActions.clearDriversProfilesUiStatuses, (state) => adapter.removeAll(state)),
  on(DriversProfilesUiStatusesActions.deleteDriversProfilesUiStatuses, (state, action) => adapter.removeMany(action.ids, state)),
  on(DriversProfilesUiStatusesActions.upsertDriversProfilesUiStatuses, (state, action) => adapter.upsertMany(action.driversProfilesUiStatuses, state)),
  on(DriversProfilesUiStatusesActions.updateDriversProfilesUiStatuses, (state, action) => adapter.updateMany(action.driversProfilesUiStatuses, state)),
);

export function reducer(state: DriversProfilesUiStatusesState | undefined, action: DriversProfilesUiStatusesActions.ActionsTypeGroup): DriversProfilesUiStatusesState {
  return driverProfileStatusReducer(state, action);
}

export const selectDriversProfilesStatusesState = createFeatureSelector<DriversProfilesUiStatusesState>(driversProfilesUiStatusesFeatureKey);

export const { selectIds, selectEntities, selectAll, selectTotal } = adapter.getSelectors(selectDriversProfilesStatusesState);

export const selectActiveDriverId = createSelector(
  selectDriversProfilesStatusesState,
  (state: DriversProfilesUiStatusesState) => state.activeDriverId
);

export const selectActiveDriverBasicInfoX = createSelector( //x-store selector
  fromDriversStore.selectEntities,
  selectActiveDriverId,
  (driversEntities: Dictionary<Driver>, activeDriverId: string) => driversEntities[activeDriverId] as Driver
);

export const selectActiveDriverRacingHistoryEntriesX = createSelector( //x-store selector
  fromDriversRacingHistoriesStore.selectEntities,
  selectActiveDriverId,
  (driversEntities: Dictionary<DriverRacingHistory>, activeDriverId: string) => driversEntities[activeDriverId]?.entries
);

export const selectUiStatusById = createSelector(
  selectEntities,
  (entities: Dictionary<DriverProfileUiStatus>, driverId: string) => entities[driverId] as DriverProfileUiStatus
);

export const selectActiveUiStatus = createSelector(
  selectEntities,
  selectActiveDriverId,
  (entities: Dictionary<DriverProfileUiStatus>, activeDriverId: string) => selectUiStatusById.projector(entities, activeDriverId)
);

export const selectActiveDriverRacingHistory = createSelector(
  selectActiveUiStatus,
  (driverProfileUiStatus: DriverProfileUiStatus) => driverProfileUiStatus?.racingHistoryGrid
);

export const selectActiveRacingHistoryColumnStates = createSelector(
  selectActiveDriverRacingHistory,
  (activeDriverRacingHistory) => activeDriverRacingHistory?.columnStates
);

export const selectActiveRacingHistorySpecificColumnState = createSelector(
  selectActiveRacingHistoryColumnStates,
  (columnStates: Map<string, ColumnState<EDriverRacingHistoryColumnIds>>, columnId: string) => columnStates?.get(columnId)
);

export const selectActiveRacingHistoryColumnSortMode = createSelector(
  selectActiveRacingHistorySpecificColumnState,
  (columnState: ColumnState<EDriverRacingHistoryColumnIds>) => columnState?.sortMode
);

export const selectActiveRacingHistoryColumnFilterTerm = createSelector(
  selectActiveRacingHistorySpecificColumnState,
  (columnState: ColumnState<EDriverRacingHistoryColumnIds>) => columnState?.filterTerm
);

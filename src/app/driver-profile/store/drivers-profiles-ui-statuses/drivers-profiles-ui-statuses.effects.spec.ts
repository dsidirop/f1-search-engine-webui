/* eslint-disable @typescript-eslint/no-explicit-any */
import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';
import { DriversProfilesUiStatusesEffects } from './drivers-profiles-ui-statuses.effects';

describe('DriversProfilesUiEffects', () => {
  let actions$: Observable<any>;
  let effects: DriversProfilesUiStatusesEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        DriversProfilesUiStatusesEffects,
        provideMockActions(() => actions$)
      ]
    });

    effects = TestBed.get<DriversProfilesUiStatusesEffects>(DriversProfilesUiStatusesEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });
});

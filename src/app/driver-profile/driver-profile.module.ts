import { ScrollingModule } from '@angular/cdk/scrolling';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '@app-shared';
import { DriverInfoComponent, DriverProfileComponent, DriverRacingHistoryGridComponent } from './components';
import { DriverProfileRoutingModule } from './driver-profile-routing.module';
import { DriverProfileStoreModule } from './driver-profile-store.module';

@NgModule({
  imports: [
    FormsModule,
    SharedModule,
    ScrollingModule,
    //store
    DriverProfileStoreModule,
    //routing
    DriverProfileRoutingModule
  ],
  declarations: [
    //smarties
    DriverProfileComponent,
    //dumies
    DriverInfoComponent,
    DriverRacingHistoryGridComponent
  ]
})

export class DriverProfileModule { }

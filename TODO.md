# F1 Search Engine

SPA for reviewing the racing-history of each F1 pilot

### Todo

- [ ] Refine the README.md file to show what code to see first. Add explicit links to the most difficult, impressive parts. Also add links to the main business logic and software tests. Ensure that the mission statement of the repo makes sense. See my READMEs on these 3 repos: https://lnkd.in/df7Aj7s, https://lnkd.in/dSMzCqt, https://lnkd.in/dhzETRW.
- [ ] rename partial scss files to _foo.scss format
- [ ] use SMACScss architecture for the SCSS
- [ ] try to use [routerLink] even if its not really being employed
- [ ] On a mobile-sized viewport we must display small F1 logo on the top left + a hamburger menu on the top right!
- [ ] translations
- [ ] e2e tests + gitlab integration!
- [ ] Try to replace this.globalConfigurationService.xyz' inside our ngrx effects with values$ from the store
- [ ] Use '[FunVal](https://github.com/neuledge/funval)' for model validation
- [ ] (de)normalize race-track-info + racing-events + car-constructor-info and other stuff like that so that the stores will be indeed flat without any data-duplication and/or nested objects
- [ ] Integrate gestures for mobile phones in order to swipe back and forth: https://medium.com/angular-in-depth/gestures-in-an-angular-application-dde71804c0d0 + Include gifs in README.md illustrating how swiping works
- [ ] f1-racing-history-entry-scrollbar-slug should be hidden away if the cdn-viewport doesn't use a scrollbar  
- [ ] It would be interesting to include an option in a driver-profile section to include the races which the driver could have participated in during his career but didn't. To achieve this we need to bring over all the races that ever happened ofcours. We then need to find the races which happened between the first and the last race the driver took part in. And then find which races the driver didn't show up for.  
- [ ] Calculate Cdn-Viewport Dynamically Based on the Viewport Height  
- [ ] Consider replacing 'ngUnsubscribe' with 'Subscription'  
- [ ] Driver Profile support for flags via https://github.com/lipis/flag-icon-css  
- [ ] Double-check the optimizations list from: https://itnext.io/improve-your-angular-apps-performance-66032f63dc09  

### In Progress

- [ ] SCSS: use variables for colours etc (theming!)  
- [ ] Separate selectors into their own .selectors.ts files  
- [ ] Introduce NgRx Facades  

### Done ✓

- [x] ⭐ Verify that ngrx-store serialization works as intended in production!  
- [x] ⭐Speed up initial loading times via hot-preloading for driver-infos, driver-racing-history, race-infos, driver-race-results. It's important to take into 2 more things in the implementation: Cache-versioning + Timestamps. We need to look into how we can load & persist the ngrx store [into IndexedDB](https://medium.com/whozapp/use-indexeddb-with-ngrx-for-fast-angular-bootstrap-d9900f0cbc1b)  
- [x] ⭐ Bru + Mark Blundell = not displaying  
- [x] ⭐ Introduce transducers wherever .map() and .filter() are involved!  
- [x] ⭐ Employ function composition ala: import flow from 'lodash/fp/flow  const formattedDates = dates.map(flow(addFiveYears, dateToString, toUpper))'  
- [x] ⭐ Debounce Column Sorting / Filtering  
- [x] Driver Profile Grid: Deburr-based filtering  
- [x] Use separate dto models for the raw-api vs the application + automapper between the two  
- [x] Transform dates on data-services! + format the date accordingly on the grid  
- [x] Nationality: Ellipsis-text + nowrap  
- [x] ⭐ Remove any and all 'any' usages  
- [x] ⭐ Consolidate dto converters / mappers  
- [x] ⭐ Fix dancing dots animation issue with firefox  
- [x] ⭐Explore optimizations on *ngFor via track-by  
- [x] ngx-quicklink: profile module must be lazy-loaded for the sake of performance + eager pre-loading of section-modules!  
- [x] Refine 404/not-found error handling in our @Effects classes  
- [x] Guard: Prohibit access to the /error page if no error has been set  
- [x] NgRx DevTools polyfill for Map and Set jsonificiation https://github.com/ngrx/store-devtools/issues/64#issuecomment-314889392  
- [x] turn _cloneDeep into a utility method  
- [x] Grid Filtering  
- [x] Grid Multi-Column Sorting  
- [x] Actually Implement Grid-Sorting  
- [x] Shrink Sidebar (with animation) when loading the profile view  
- [x] Separate App-Sidebar Component  

